$(function () {
    //修改确认框的文本
    $.messager.model = {
        ok: {text: '确认'},
        cancel: {text: '取消'}
    }
});

function messageHandler(data) {
    if (data.success) {
        //带标题的提示框   title标题,提示的信息
        $.messager.alert("温馨提示:", "操作成功!2秒后自动刷新页面");
        setTimeout(function () {
            window.location.reload();
        }, 1000);
    } else {
        $.messager.popup(data.msg);
    }
}
//处理使用 jQuery 发送ajax请求处理数组参数传递的问题
//禁用数组添加[]的功能
jQuery.ajaxSettings.traditional = true;