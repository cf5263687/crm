﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>客户管理</title>
    <!--freemarker引入模板文件 使用相对路径来引入-->
    <#include "../common/link.ftl" />
    <script>
        $(function () {
            //新增和编辑
            $(".btn-input").click(function () {
                //如果先点击了编辑,那么模态框中就有值了,这时再去点击新增时,就会有问题
                //所以每次点击后,都先把模态框中的值清空
                $("#editForm input,select").val("");
                //获取data-json的值   $花括号 根据对象的get方法获取属性值
                var json = $(this).data("json");
                //console.log(json);
                if (json) {//如果json有值,说明时编辑操作,进行数据回显
                    $("#editForm input[name=id]").val(json.id);
                    $("#editForm input[name=name]").val(json.name);
                    $("#editForm input[name=age]").val(json.age);
                    $("#editForm select[name=gender]").val(json.gender);
                    $("#editForm input[name=tel]").val(json.tel);
                    $("#editForm input[name=qq]").val(json.qq);
                    $("#editForm select[name='job.id']").val(json.jobId);
                    $("#editForm select[name='source.id']").val(json.sourceId);
                }
                //弹出模态框
                $("#editModal").modal("show");
            });
            //表单验证  bootstrapValidator插件
             $("#editForm").bootstrapValidator({
                 feedbackIcons: { //反馈图标
                     valid: 'glyphicon glyphicon-ok',
                     invalid: 'glyphicon glyphicon-remove',
                     validating: 'glyphicon glyphicon-refresh'
                 },
                 fields: {//配置要验证的字段
                     tel: {
                         validators: {
                             phone: {
                                 country: "CN",
                                 message: "电话格式不正确"
                             },
                             remote: { //远程验证
                                 type: 'POST', //请求方式
                                 url: '/customer/checkTel.do', //请求地址
                                 message: '电话号码已经存在', //验证不通过时的提示信息
                                 delay: 1000, //输入内容1秒后发请求
                                 data: function () {  //自定义提交参数，默认只会提交当前用户名input的参数
                                     return {
                                         id: $('[name="id"]').val(),
                                         tel: $('[name="tel"]').val()
                                     };
                                 }
                             }
                         }
                     }
                 }
             }).on('success.form.bv', function (e) { //表单所有数据验证通过后执行里面的代码
                 //禁止原本的表单提交
                 e.preventDefault();
                 //提交异步表单
                 $("#editForm").ajaxSubmit(function (data) {
                     if (data.success) {
                         //带标题的提示框   title标题,提示的信息
                         $.messager.alert("温馨提示:", "操作成功!2秒后自动刷新页面");
                         setTimeout(function () {
                             window.location.href = "/customer/potentialList.do";
                         }, 2000);
                     } else {
                         $.messager.alert(data.msg);
                     }
                 });
             });

            //给保存按钮绑定点击事件
           /* $(".btn-submit").click(function () {
                //当发送异步请求中含有form表单时,可以使用jQuery-form插件进行异步提交
                $("#editForm").ajaxSubmit(messageHandler);
            });*/

            //删除操作
            $(".btn-delete").click(function () {
                //获取data-id的值   也就是客户对象的id值
                var id = $(this).data("id");
                //确认框   title标题,需要用户确认的信息,点击确认后的回调函数
                $.messager.confirm("警告:", "您确定要删除这条数据吗?", function () {
                    //messageHandler  是抽取出来的函数 在js/system/commonAll.js中
                    $.get('/customer/delete.do', {id: id}, messageHandler)
                });
            });
            //----------------------跟进-------------------------------
            //跟进按钮点击事件,弹出模态框
            $(".btn-trace").click(function () {
                var json = $(this).data("json");
                //给跟进模态框设置客户姓名
                $("#traceForm input[name='customer.name']").val(json.name);
                //给跟进模态框设置客户id
                $("#traceForm input[name='customer.id']").val(json.id);
                $("#traceModal").modal('show');
            });
            //跟进时间
            $("#traceForm input[name=traceTime]").datepicker({
                todayBtn: "linked",
                clearBtn: true,
                language: "zh-CN",
                autoclose: true,
                todayHighlight: true
            });

            //跟进表单提交
            $(".trace-submit").click(function () {
                $("#traceForm").ajaxSubmit(messageHandler);
            });
            //-----------------------移交---------------------------------
            $(".btn-transfer").click(function () {
                var json = $(this).data('json'); //json是客户对象
                $("#transferForm input[name='customer.name']").val(json.name);
                $("#transferForm input[name='customer.id']").val(json.id);
                $("#transferForm input[name='oldSeller.name']").val(json.sellerName);
                $("#transferForm input[name='oldSeller.id']").val(json.sellerId);
                $("#transferModal").modal('show');
            });

            $(".transfer-submit").click(function () {
                $("#transferForm").ajaxSubmit(messageHandler);
            });
        });
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl"/>
    <!-- 菜单回显-->
    <#assign currentMenu="customer_potential"/>
    <#include "../common/menu.ftl"/>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>客户管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <form class="form-inline" id="searchForm" action="/customer/potentialList.do" method="post">
                    <input type="hidden" name="currentPage" id="currentPage" value="1">

                    <div class="form-group">
                        <label for="keyword">关键字:</label>
                        <input type="text" class="form-control" id="keyword" name="keyword" value="${(qo.keyword)!}"
                               placeholder="请输入姓名/电话">
                    </div>
                    <@shiro.hasAnyRoles name="Admin,Market_Manager">
                    <div class="form-group">
                        <label for="seller"> 销售人员:</label>
                        <select class="form-control" id="seller" name="sellerId">
                            <option value="-1">全部</option>
                                <#list sellers! as seller>
                                    <option value="${seller.id}">${seller.name}</option>
                                </#list>
                        </select>
                        <script>
                            <#-- $("#seller option[value='${(qo.sellerId)!}']").prop("selected", true);-->
                            $("#seller").val(${(qo.sellerId)!});
                        </script>
                    </div>
                    </@shiro.hasAnyRoles>
                    <button id="btn_query" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>
                        查询
                    </button>

                    <a href="#" class="btn btn-success btn-input" style="margin: 10px">
                        <span class="glyphicon glyphicon-plus"></span> 添加
                    </a>
                </form>
                <!--编写内容-->
                <div class="box-body table-responsive no-padding ">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th>编号</th>
                            <th>姓名</th>
                            <th>电话</th>
                            <th>QQ</th>
                            <th>职业</th>
                            <th>来源</th>
                            <th>销售员</th>
                            <th>录入时间</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                        <#list pageInfo.list as customer>
                            <tr>
                            <#-- 序号列    索引值从0开始,所以需要+1 -->
                                <td>${customer_index+1}</td>
                                <td>${customer.name!}</td>
                                <td>${customer.tel!}</td>
                                <td>${customer.qq!}</td>
                                <td>${(customer.job.title)!}</td>
                                <td>${(customer.source.title)!}</td>
                                <td>${(customer.seller.name)!}</td>
                                <td>${(customer.inputTime?string('yyyy-MM-dd'))!}</td>
                                <td>${customer.statusName!}</td>
                                <td>
                                    <a href="#" class="btn btn-info btn-xs btn-input"
                                       data-json='${customer.json!}'>
                                        <span class="glyphicon glyphicon-pencil"></span> 编辑
                                    </a>
                                    <a href="#" class="btn btn-danger btn-xs btn-trace"
                                       data-json='${customer.json!}'>
                                        <span class="glyphicon glyphicon-phone"></span> 跟进
                                    </a>
                                    <!--管理员和经理才能看到该下拉框-->
                                    <@shiro.hasAnyRoles name="Admin,Market_Manager">
                                        <a href="#" class="btn btn-danger btn-xs btn-transfer"
                                           data-json='${customer.json!}' >
                                            <span class="glyphicon glyphicon-phone"></span> 移交
                                        </a>
                                    </@shiro.hasAnyRoles>
                                </td>
                            </tr>
                        </#list>
                    </table>
                    <!--分页-->
                    <#include "../common/page.ftl"/>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- 新增/编辑模态框 -->
<div class="modal fade" id="editModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title inputTitle">客户编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/customer/saveOrUpdate.do" method="post" id="editForm">
                    <input type="hidden" value="" name="id">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户名称：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name"
                                   placeholder="请输入客户姓名"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户年龄：</label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" name="age"
                                   placeholder="请输入客户年龄"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户性别：</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="gender">
                                <option value="1">男</option>
                                <option value="0">女</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户电话：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="tel"
                                   placeholder="请输入客户电话"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户QQ：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="qq"
                                   placeholder="请输入客户QQ"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户工作：</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="job.id">
                                <#list jobs as job>
                                    <option value="${job.id}">${job.title}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">客户来源：</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="source.id">
                                <#list sources as source>
                                    <option value="${source.id}">${source.title}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-submit">保存</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<#--跟进模态框-->
<div class="modal fade" id="traceModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">跟进</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/customerTraceHistory/saveOrUpdate.do" method="post"
                      id="traceForm">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">客户姓名：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="customer.name" readonly><#--给用户看的-->
                            <input type="hidden" name="customer.id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">跟进时间：</label>
                        <div class="col-lg-6 ">
                            <input type="text" class="form-control" name="traceTime" placeholder="请输入跟进时间">

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">交流方式：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="traceType.id">
                                <#list ccts! as c>
                                    <option value="${c.id}">${c.title}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">跟进结果：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="traceResult">
                                <option value="3">优</option>
                                <option value="2">中</option>
                                <option value="1">差</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">跟进记录：</label>
                        <div class="col-lg-6">
                            <textarea type="text" class="form-control" name="traceDetails"
                                      placeholder="请输入跟进记录" name="remark"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary trace-submit">保存</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<#--移交模态框-->
<div id="transferModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">移交</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/customerTransfer/saveOrUpdate.do" method="post" id="transferForm"
                      style="margin: -3px 118px">
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">客户姓名：</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="customer.name" readonly>
                            <input type="hidden" class="form-control" name="customer.id">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sn" class="col-sm-4 control-label">旧营销人员：</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="oldSeller.name" readonly>
                            <input type="hidden" class="form-control" name="oldSeller.id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sn" class="col-sm-4 control-label">新营销人员：</label>
                        <div class="col-sm-8">
                            <select name="newSeller.id" class="form-control">
                                <#list sellers! as seller>
                                    <option value="${seller.id}">${seller.name}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sn" class="col-sm-4 control-label">移交原因：</label>
                        <div class="col-sm-8">
                            <textarea type="text" class="form-control" id="reason" name="reason" cols="10"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary transfer-submit">保存</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
