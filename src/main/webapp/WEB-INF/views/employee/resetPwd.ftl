<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>重置密码</title>
    <#include "../common/link.ftl" >
    <script>
        $(function () {
            $('#submitBtn').click(function () {
                //var params=$('#editForm').serialize();
                var id = $("input[name=id]").val();
                var name = ${name};
                var newPassword = $("input[name=newPassword]").val();

                $.post('/employee/resetPwd.do',{id:id,name:name,newPassword:newPassword},function (data) {
                    if (data.success){//重置成功
                        alert(data.msg)
                        window.location.href="/employee/list.do";
                    }else{//重置失败
                        alert(data.msg);
                    }
                })
            })
        });
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl" >
    <!--菜单回显-->
    <#assign currentMenu="employee">
    <#include "../common/menu.ftl" >
    <div class="content-wrapper">
        <section class="content-header">
            <h1>重置密码</h1>
        </section>
        <section class="content">
            <div class="box" style="padding: 30px;" >
                <!--高级查询--->
                <form class="form-horizontal" action="/employee/resetPwd.do" method="post" id="editForm" >
                    <input type="hidden"  name="id" value="${id}">
                    <div class="form-group" style="text-align: center;">
                       <h3>您正在重置员工 &nbsp;${name}&nbsp; 的密码</h3>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">默认密码：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control"  name="newPassword" value="123456"  readonly="readonly"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button id="submitBtn" type="button" class="btn btn-primary">确定重置</button>
                        </div>
                    </div>
                </form>

            </div>
        </section>
    </div>
    <#include "../common/footer.ftl" >
</div>
</body>
</html>
