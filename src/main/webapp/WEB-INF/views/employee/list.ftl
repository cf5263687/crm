<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>员工管理</title>
    <#include "../common/link.ftl"/>
    <script>
        $(function () {
            var $allCb = $("#allCb");
            var $cb = $(".cb");
            //给allCb复选框添加点击事件
            $allCb.click(function () {
                //将所有cb复选框状态设置的与allCb同步
                $cb.each(function (index, item) {
                    $(item).prop("checked", $allCb.prop("checked"));
                })
            });
            //给每个cb复选框添加点击事件,
            //当cb复选框的选中数量和cb复选框的总数量相等时,allCb也要选中,不相等时allCb不选中
            $cb.click(function () {
                $allCb.prop("checked", $cb.length == $(".cb:checked").length);
            });

            //给批量添加按钮添加点击事件
            $(".btn_batchDelete").click(function () {
                //如果没有复选框选中,提示用户先勾选
                if ($(".cb:checked").length == 0) {
                    $.messager.popup("请先勾选要删除的数据!")
                } else {
                    $.messager.confirm("警告:", "您确定要删除这些数据吗?", function () {
                        var ids = [];
                        //获取被选中的复选框的员工id值,并传入数组中
                        $(".cb:checked").each(function (index, item) {
                            ids.push($(item).data("id"));
                        });
                        // jQuery 发送ajax请求处理数组参数时,参数为ids[]=1,ids[]=2...
                        //属性名上带着[]  解决方法在
                        $.post("/employee/batchDelete.do", {ids: ids}, messageHandler);
                    })
                }
            })

            //给禁用/恢复按钮添加点击事件
            $(".btn_updateStatus").click(function () {
                //获取id和status
                var id = $(this).data("id");

                $.get("/employee/updateStatus.do", {id: id}, messageHandler);
            });
            //给导入按钮添加点击事件
            $(".btn-import").click(function () {
               $("#importModal").modal("show");
            });

            $(".btn-submit").click(function () {
                $("#importForm").ajaxSubmit(messageHandler)
            })


        })
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl" />
    <!--菜单回显-->
    <#assign currentMenu="employee" />
    <#include "../common/menu.ftl"/>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>员工管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <div style="margin: 10px;">
                    <form class="form-inline" id="searchForm" action="/employee/list.do" method="post">
                        <input type="hidden" name="currentPage" id="currentPage" value="1">
                        <div class="form-group">
                            <label for="keyword">关键字:</label>
                            <input type="text" class="form-control" id="keyword" name="keyword" value="${(qo.keyword)!}"
                                   placeholder="请输入姓名/邮箱">
                        </div>
                        <div class="form-group">
                            <label for="dept"> 部门:</label>
                            <select class="form-control" id="dept" name="deptId">
                                <option value="-1">全部</option>
                                <#list departments! as dept>
                                    <option value="${dept.id}">${dept.name}</option>
                                </#list>
                            </select>
                            <script>
                                $("#dept option[value='${(qo.deptId)!}']").prop("selected", true);
                            </script>
                        </div>
                        <button id="btn_query" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>
                            查询
                        </button>
                        <a href="/employee/input.do" class="btn btn-success btn_redirect">
                            <span class="glyphicon glyphicon-plus"></span> 添加
                        </a>
                        <a href="#" class="btn btn-danger btn_batchDelete">
                            <span class="glyphicon glyphicon-trash"></span> 批量删除
                        </a>
                        <a href="/employee/exportXls.do" class="btn btn-warning" >
                            <span class="glyphicon glyphicon-download"></span> 导出
                        </a>
                        <a href="#" class="btn btn-warning btn-import">
                            <span class="glyphicon glyphicon-upload"></span> 导入
                        </a>
                    </form>
                </div>
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th><input type="checkbox" id="allCb"></th>
                        <th>编号</th>
                        <th>名称</th>
                        <th>email</th>
                        <th>年龄</th>
                        <th>部门</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <#list pageInfo.list as employee>
                        <tr>
                            <td><input type="checkbox" class="cb" data-id="${employee.id}"></td>
                            <td>${employee_index+1}</td>
                            <td>${(employee.name)!}</td>
                            <td>${(employee.email)!}</td>
                            <td>${(employee.age)!}</td>
                            <td>${(employee.dept.name)!}</td>
                        <#--  <td>${employee.status?string("正常","禁用")}</td>-->
                            <#if employee.status>
                                 <td>正常</td>
                            </#if>
                             <#if !employee.status>
                                 <td>禁用</td>
                             </#if>
                            <td>
                                <a href="/employee/input.do?id=${employee.id}"
                                   class="btn btn-info btn-xs btn_redirect">
                                    <span class="glyphicon glyphicon-pencil"></span> 编辑
                                </a>
                            <#--判断EMPLOYEE_IN_SESSION不为空 且 EMPLOYEE_IN_SESSION.admin为true-->
                            <#--<#if EMPLOYEE_IN_SESSION.admin>-->
                                <@shiro.hasRole name="Admin"><#--是否拥有Admin的角色-->
                                    <a href="/employee/inputPwd.do?id=${employee.id}&name=${employee.name}"
                                        class="btn btn-primary btn-xs btn_resetPwd">
                                        <span class="glyphicon glyphicon-repeat"></span> 重置密码
                                    </a>
                                    <#if employee.status>
                                        <a href="#" data-id="${employee.id}"
                                           class="btn btn-warning btn-xs btn_updateStatus">
                                            <span class="glyphicon glyphicon-tags"></span> 禁用
                                        </a>
                                    </#if>
                                    <#if !employee.status >
                                        <a href="#" data-id="${employee.id}"
                                           class="btn btn-success btn-xs btn_updateStatus">
                                            <span class="glyphicon glyphicon-tags"></span> 恢复
                                        </a>
                                    </#if>
                                </@shiro.hasRole>
                            <#-- </#if>-->
                                <a href="/employee/delete.do?id=${employee.id}"
                                   class="btn btn-danger btn-xs btn_delete">
                                    <span class="glyphicon glyphicon-trash"></span> 删除
                                </a>
                            </td>
                        </tr>
                    </#list>
                </table>
                <!--分页-->
                <#include "../common/page.ftl"/>
            </div>
        </section>
    </div>
    <#include "../common/footer.ftl" />
</div>
<#--模态框-->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">导入</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/employee/importXls.do" method="post" id="importForm">
                    <input type="hidden" name="id">
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label"></label>
                        <div class="col-sm-6">
                            <input type="file" name="file">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            <a href="/xlstemplates/employee_import.xls" class="btn btn-success" >
                                <span class="glyphicon glyphicon-download"></span> 下载模板
                            </a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary btn-submit">保存</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
