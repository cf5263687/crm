<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>员工管理</title>
    <#include "../common/link.ftl" >
    <script>
        //分配角色 左移右移
        function moveSelected(srcClass, targetClass) {
            $("." + targetClass).append($("." + srcClass + " option:selected"));
        };

        function moveAll(srcClass, targetClass) {
            $("." + targetClass).append($("." + srcClass + " option"));
        };

        //页面加载完
        //如果是超级管理员就不需要显示角色管理选项
        $(function () {
            var roleDiv;
            //给超级管理员的复选框添加点击事件
            $("#admin").click(function () {
                var flag = $(this).prop("checked");
                if (flag) {
                    //选中状态 分配角色 隐藏
                    roleDiv = $("#role").detach(); //detach删除后保留原有事件 remove删除后不保留事件
                } else {
                    //恢复  分配角色
                    $("#adminDiv").after(roleDiv);
                }
            });
            //访问编辑页面时,由于没有触发点击事件,所以超级管理员时还会显示  分配角色
            //所以设置页面加载完后获取超级管理员复选框状态
            var flag = $("#admin").prop("checked");
            if (flag) {
                //设置隐藏  分配角色
                roleDiv = $("#role").detach();
            }//默认是显示的,所以不用写else的情况

            //角色回显的时候，左右两边的角色有重复，应该是有右边有的角色，不应该在左边出现。
            var ids = [];//创建一个数组
            $(".selfRoles option").each(function (index, item) {
                ids.push($(item).val());    //把右侧的角色选项的value值(角色id值)存入数组中
            });
            $(".allRoles option").each(function (index, item) {  //遍历左侧所有的角色
                var id = $(item).val();
                //返回值:Number jQuery.inArray(value,array)
                //返回参数在数组中的位置，从0开始计数(如果没有找到则返回 -1 )。
                if ($.inArray(id, ids) > -1) {  //判断是否存在ids数组中,如果是就删除掉自己
                    $(item).remove();
                }
            })

            //解决表达那提交时的,角色选中问题
            /* $("#submitBtn").click(function () {
                 //把右侧的表单内容状态改成全部选中
                 $(".selfRoles option").prop("selected",true);
                 //提交表单
                 $("#editForm").submit();
             });*/

            //表单验证  bootstrapValidator插件
            $('#editForm').bootstrapValidator({
                feedbackIcons: { //反馈图标
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {//配置要验证的字段
                    name: {//用户名
                        //group: '.col-lg-4',
                        validators: {//验证的规则
                            notEmpty: {
                                message: '用户名不能为空'  //发生错误时的提示消息
                            },
                            stringLength: {//字符串长度
                                min: 1,
                                max: 8,
                                //message:'用户名的长度在1到8之间'    //不写时有默认的提示消息
                            },
                            /* regexp: {//正则表达式
                                 regexp: /^[a-zA-Z0-9_\.]+$/,
                                 message: '用户名只能由字母、数字、点和下划线组成'
                             },*/
                            different: {//不同的   意思是与下面的password,repassword字段值不能相同
                                field: 'password,repassword',
                                message: '用户名和密码不能相同'
                            },
                            /*插件要求返回结果需要为键值对形式 key为valid  ，value为boolean类型
                            valid : true  代表验证通过(该用户名不存在)
                            valid：false  代表验证不通过(用户名已经存在)*/
                            remote: { //远程验证
                                type: 'POST', //请求方式
                                url: '/employee/checkName.do', //请求地址
                                message: '用户名已经存在', //验证不通过时的提示信息
                                delay: 1000, //输入内容1秒后发请求
                                data: function() {  //自定义提交参数，默认只会提交当前用户名input的参数
                                    return {
                                        id: $('[name="id"]').val(),
                                        name: $('[name="name"]').val()
                                    };
                                }
                            }
                        }
                    },
                    password: {//密码
                        validators: {//验证规则
                            notEmpty: {
                                message: '密码是必需的，不能为空'
                            },
                            identical: {//相同的   和下面的repassword字段要完全相同
                                field: 'repassword',
                                message: '输入的密码不一致'
                            },
                            different: {//不同的   与下面的username字段值不能相同
                                field: 'name',
                                message: '密码不能与用户名相同'
                            }
                        }
                    },
                    repassword: {
                        validators: {
                            notEmpty: {
                                message: '密码是必需的，不能为空'
                            },
                            identical: {//相同的   和下面的password字段要完全相同
                                field: 'password',
                                message: '输入的密码不一致'
                            },
                            different: {//不同的   与下面的username字段值不能相同
                                field: 'name',
                                message: '密码不能与用户名相同'
                            }
                        }
                    },
                    email: {//邮箱
                        validators: {
                            emailAddress: { //邮箱格式  都不用写正则,他自己判断
                                message: '输入的不是有效的电子邮件地址'
                            }
                        }
                    },
                    age: {
                        validators: {
                            between: { //数字的范围
                                min: 18,
                                max: 60
                            }
                        }
                    }
                }
                //使用验证插件自带的提交方式     之前的提交已经不能用了,点击提交只会进行验证
                //把按钮改为submit类型，并且放在form表单内
                // 去掉之前button时绑定的点击事件   只能使用异 步提交的方式
            }).on('success.form.bv', function (e) { //表单所有数据验证通过后执行里面的代码
                //禁止原本的表单提交
                e.preventDefault();
                //设置右边的所有option为选中的状态
                $(".selfRoles option").prop('selected', true);
                //提交异步表单
                $("#editForm").ajaxSubmit(function (data) {
                    if (data.success) {
                        //带标题的提示框   title标题,提示的信息
                        $.messager.alert("温馨提示:", "操作成功!2秒后自动刷新页面");
                        setTimeout(function () {
                            window.location.href="/employee/list.do";
                        }, 2000);
                    } else {
                        $.messager.alert(data.msg);
                    }
                })
            });
        });

    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl" >
    <!--菜单回显-->
    <#assign currentMent="employee">
    <#include "../common/menu.ftl" >
    <div class="content-wrapper">
        <section class="content-header">
            <h1>员工编辑</h1>
        </section>
        <section class="content">
            <div class="box">
                <form class="form-horizontal" action="/employee/saveOrUpdate.do" method="post" id="editForm">
                    <input type="hidden" value="${(employee.id)!}" name="id">
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-2 control-label">用户名：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" name="name" value="${(employee.name)!}"
                                   placeholder="请输入用户名">
                        </div>
                    </div>
                <#--??判断对象有值 取反表示对象为空-->
                    <#if !employee??>
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">密码：</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="password" name="password"
                                       placeholder="请输入密码">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="repassword" class="col-sm-2 control-label">验证密码：</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="repassword" name="repassword"
                                       placeholder="再输入一遍密码">
                            </div>
                        </div>
                    </#if>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">电子邮箱：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="email" name="email" value="${(employee.email)!}"
                                   placeholder="请输入邮箱">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="age" class="col-sm-2 control-label">年龄：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="age" name="age" value="${(employee.age)!}"
                                   placeholder="请输入年龄">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="dept" class="col-sm-2 control-label">部门：</label>
                        <div class="col-sm-6">
                            <select class="form-control" id="dept" name="dept.id">
                                <#list departments! as d>
                                    <option value="${d.id}" }>${d.name}</option>
                                </#list>
                            </select>
                            <script>
                                $("#dept").val(${(employee.dept.id)!})
                            </script>
                        </div>
                    </div>
                    <div class="form-group" id="adminDiv">
                        <label for="admin" class="col-sm-2 control-label">超级管理员：</label>
                        <div class="col-sm-6" style="margin-left: 15px;">
                            <input type="checkbox" id="admin" name="admin" class="checkbox">
                        <#-- 超级管理员的回显   employee有值 且 employee是管理员-->
                            <#if employee?? && employee.admin>
                                <script>
                                    $("#admin").prop("checked", true);
                                </script>
                            </#if>
                        </div>
                    </div>
                    <div class="form-group " id="role">
                        <label for="role" class="col-sm-2 control-label">分配角色：</label><br/>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-sm-2 col-sm-offset-2">
                                <select multiple class="form-control allRoles" size="15">
                                    <#list roles as role>
                                        <option value="${role.id}">${role.name}</option>
                                    </#list>
                                </select>
                            </div>

                            <div class="col-sm-1" style="margin-top: 60px;" align="center">
                                <div>

                                    <a type="button" class="btn btn-primary  " style="margin-top: 10px" title="右移动"
                                       onclick="moveSelected('allRoles', 'selfRoles')">
                                        <span class="glyphicon glyphicon-menu-right"></span>
                                    </a>
                                </div>
                                <div>
                                    <a type="button" class="btn btn-primary " style="margin-top: 10px" title="左移动"
                                       onclick="moveSelected('selfRoles', 'allRoles')">
                                        <span class="glyphicon glyphicon-menu-left"></span>
                                    </a>
                                </div>
                                <div>
                                    <a type="button" class="btn btn-primary " style="margin-top: 10px" title="全右移动"
                                       onclick="moveAll('allRoles', 'selfRoles')">
                                        <span class="glyphicon glyphicon-forward"></span>
                                    </a>
                                </div>
                                <div>
                                    <a type="button" class="btn btn-primary " style="margin-top: 10px" title="全左移动"
                                       onclick="moveAll('selfRoles', 'allRoles')">
                                        <span class="glyphicon glyphicon-backward"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <select multiple class="form-control selfRoles" size="15" name="ids">
                                    <#list (employee.roles)! as empRole>
                                        <option value="${(empRole.id)!}">${(empRole.name)!}</option>
                                    </#list>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-1 col-sm-6">
                            <button id="submitBtn" type="submit" class="btn btn-primary">保存</button>
                            <button type="reset" class="btn btn-danger">重置</button>
                        </div>
                    </div>

                </form>

            </div>
        </section>
    </div>
    <#include "../common/footer.ftl" >
</div>
</body>
</html>
