﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>跟进历史</title>
    <!--freemarker引入模板文件 使用相对路径来引入-->
    <#include "../common/link.ftl" />
    <script>
        $(function () {
            //新增和编辑
            $(".btn-input").click(function () {
                //如果先点击了编辑,那么模态框中就有值了,这时再去点击新增时,就会有问题
                //所以每次点击后,都先把模态框中的值清空
                $("#editForm input").val("");
                $("#editForm select").val(-1);
                //获取data-json的值   $花括号 根据对象的get方法获取属性值
                var json = $(this).data("json");
                if (json) {//如果json有值,说明时编辑操作,进行数据回显
                    $("#editForm input[name=id]").val(json.id);
                    $("#editForm select[name='customer.id']").val(json.customer.id);
                    $("#editForm select[name='course.id']").val(json.course.id);
                    $("#editForm input[name=money]").val(json.money);
                }
                //弹出模态框
                $("#editModal").modal("show");
            });

            //给保存按钮绑定点击事件
            $(".btn-submit").click(function () {
                //当发送异步请求中含有form表单时,可以使用jQuery-form插件进行异步提交
                $("#editForm").ajaxSubmit(messageHandler);
            });

            //删除操作
            $(".btn-delete").click(function () {
                //获取data-id的值
                var id = $(this).data("id");
                //确认框   title标题,需要用户确认的信息,点击确认后的回调函数
                $.messager.confirm("警告:", "您确定要删除这条数据吗?", function () {
                    //messageHandler  是抽取出来的函数 在js/system/commonAll.js中
                    $.post('/customerTraceHistory/delete.do', {id: id}, messageHandler)
                });
            });
        });
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl"/>
    <!-- 菜单回显-->
    <#assign currentMenu="customerTraceHistory"/>
    <#include "../common/menu.ftl"/>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>跟进历史</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <form class="form-inline" id="searchForm" action="/customerTraceHistory/list.do" method="post">
                    <input type="hidden" name="currentPage" id="currentPage" value="1">
                    <div class="form-group">
                        <label for="keyword">关键字:</label>
                        <input type="text" class="form-control" id="keyword" name="keyword" value="${(qo.keyword)!}"
                               placeholder="请输入客户名称">
                    </div>
                    <button id="btn_query" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>
                        查询
                    </button>
                </form>
                <!--编写内容-->
                <div class="box-body table-responsive no-padding ">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th>编号</th>
                            <th>客户名称</th>
                            <th>跟进日期</th>
                            <th>跟进内容</th>
                            <th>跟进方式</th>
                            <th>跟进结果</th>
                            <th>录入人</th>
                        </tr>
                        <#list (pageInfo.list)! as customerTraceHistory>
                            <tr>
                            <#-- 序号列    索引值从0开始,所以需要+1 -->
                                <td>${(customerTraceHistory_index)!+1}</td>
                                <td>${(customerTraceHistory.customer.name)!}</td>
                                <td>${(customerTraceHistory.traceTime?string('yyyy-MM-dd'))!}</td>
                                <td>${(customerTraceHistory.traceDetails)!}</td>
                                <td>${(customerTraceHistory.traceType.title)!}</td>
                                <#if (customerTraceHistory.traceResult)! == 3>
                                    <td>优</td>
                                    <#elseif (customerTraceHistory.traceResult)! == 2>
                                    <td>中</td>
                                    <#else>
                                    <td>差</td>
                                </#if>
                                <td>${(customerTraceHistory.inputUser.name)!}</td>
                            </tr>
                        </#list>
                    </table>
                    <!--分页-->
                    <#include "../common/page.ftl"/>
                </div>
            </div>
        </section>
    </div>
</div>
</body>
</html>
