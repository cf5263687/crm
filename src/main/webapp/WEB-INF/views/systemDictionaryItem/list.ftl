﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>字典明细管理</title>
    <!--freemarker引入模板文件 使用相对路径来引入-->
    <#include "../common/link.ftl" />
    <script>
        $(function () {
            //新增和编辑
            $(".btn-input").click(function () {
                //点击新增时,先判断一下左侧目录是否选中,若无选中,提示用户,请先选中目录
                if(!$(".list-group a").hasClass("active")){
                    $.messager.popup("请先选中目录,再点击添加哟~");
                    return;
                }
                //如果先点击了编辑,那么模态框中就有值了,这时再去点击新增时,就会有问题
                //所以每次点击后,都先把模态框中的值清空
                $("#editForm input").val("");
                //获取左侧字典目录a标签的内容,也就是字典目录标题,设置到模态框中
                $("#parentTitle").val($("#${qo.parentId}").text());
                $("#parentId").val(${qo.parentId});
                //获取data-json的值   $花括号 根据对象的get方法获取属性值
                var json = $(this).data("json");
                //console.log(json);
                if (json) {//如果json有值,说明时编辑操作,进行数据回显
                    $("#editForm input[name=id]").val(json.id);
                    $("#editForm input[name=title]").val(json.title);
                    $("#editForm input[name=sequence]").val(json.sequence);
                }
                //弹出模态框
                $("#editModal").modal("show");
            });

            //给保存按钮绑定点击事件
            $(".btn-submit").click(function () {
                //当发送异步请求中含有form表单时,可以使用jQuery-form插件进行异步提交
                $("#editForm").ajaxSubmit(messageHandler);
            });

            //删除操作
            $(".btn-delete").click(function () {
                //获取data-id的值   也就是字典明细对象的id值
                var id = $(this).data("id");
                //确认框   title标题,需要用户确认的信息,点击确认后的回调函数
                $.messager.confirm("警告:", "您确定要删除这条数据吗?", function () {
                    //messageHandler  是抽取出来的函数 在js/system/commonAll.js中
                    $.get('/systemDictionaryItem/delete.do', {id: id}, messageHandler)
                });
            });
        });
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl"/>
    <!-- 菜单回显-->
    <#assign currentMenu="systemDictionaryItem"/>
    <#include "../common/menu.ftl"/>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>字典明细管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--编写内容-->
                <div class="row" style="margin:20px">
                    <div class="col-xs-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">字典目录</div>
                            <div class="panel-body">
                                <div class="list-group">
                                  <#list systemDictionaries! as systemDictionary>
                                      <#--给a标签添加上一个动态的id值,职位字典目录的id数值,也可以保证每一个id属性不重复-->
                                      <a id="${systemDictionary.id}"
                                         href="/systemDictionaryItem/list.do?parentId=${systemDictionary.id}"
                                         class="list-group-item ">${systemDictionary.title}</a>
                                  </#list>
                                    <script>
                                        /*通过a标签的id属性获取a标签,查询的哪个就是哪个a标签,也就是qo中的id
                                        * 然后给a标签加上class属性,  达到高亮的效果*/
                                        $("#${qo.parentId}").addClass("active");
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-9">
                        <!--高级查询--->
                        <form class="form-inline" id="searchForm" action="/systemDictionaryItem/list.do"
                              method="post">
                            <input type="hidden" name="currentPage" id="currentPage" value="1">
                            <input type="hidden" name="parentId" value="${qo.parentId}"><#--解决分页时查询条件丢失-->
                            <a href="#" class="btn btn-success btn-input" style="margin: 10px">
                                <span class="glyphicon glyphicon-plus"></span> 添加
                            </a>
                        </form>
                        <div class="box-body table-responsive no-padding ">
                            <table class="table table-hover table-bordered">
                                <tr>
                                    <th>编号</th>
                                    <th>标题</th>
                                    <th>序号</th>
                                    <th>操作</th>
                                </tr>
                                <#list (pageInfo.list)! as systemDictionaryItem>
                                     <tr>
                                     <#-- 序号列    索引值从0开始,所以需要+1 -->
                                         <td>${(systemDictionaryItem_index)!+1}</td>
                                         <td>${(systemDictionaryItem.title)!}</td>
                                         <td>${(systemDictionaryItem.sequence)!}</td>
                                         <td>
                                             <a href="#" class="btn btn-info btn-xs btn-input"
                                                data-json='${(systemDictionaryItem.json)!}'>
                                                 <span class="glyphicon glyphicon-pencil"></span> 编辑
                                             </a>
                                             <a href="#" class="btn btn-danger btn-xs btn-delete"
                                                data-id="${(systemDictionaryItem.id)!}">
                                                 <span class="glyphicon glyphicon-trash"></span> 删除
                                             </a>
                                         </td>
                                     </tr>
                                </#list>
                            </table>
                            <!--分页-->
                            <#include "../common/page.ftl"/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>


<!-- 模态框 -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">新增/编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/systemDictionaryItem/saveOrUpdate.do" method="post"
                      id="editForm">
                    <input type="hidden" name="id">
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="intro" class="col-sm-3 control-label">字典目录：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="parentTitle" name="parentTitle"
                                   readonly/><#--给用户看的-->
                            <input type="hidden" id="parentId"
                                   name="parentId" <#--value="${qo.parentId}"-->><#--用来提交表单 value值放上面不容易忘-->
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="title" class="col-sm-3 control-label">明细标题：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="title" name="title"
                                   placeholder="请输入明细标题">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">明细序号：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="sequence" name="sequence"
                                   placeholder="请输入明细编码">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary btn-submit">保存</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
