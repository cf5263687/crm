﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>班级管理</title>
    <!--freemarker引入模板文件 使用相对路径来引入-->
    <#include "../common/link.ftl" />
    <script>
        $(function () {
            //新增和编辑
            $(".btn-input").click(function () {
                //如果先点击了编辑,那么模态框中就有值了,这时再去点击新增时,就会有问题
                //所以每次点击后,都先把模态框中的值清空
                $("#editForm input").val("");
                $("#editForm select").val(-1);
                //获取data-json的值   $花括号 根据对象的get方法获取属性值
                var json = $(this).data("json");
                if (json) {//如果json有值,说明时编辑操作,进行数据回显
                    $("#editForm input[name=id]").val(json.id);
                    $("#editForm input[name=name]").val(json.name);
                    $("#editForm input[name=number]").val(json.number);
                    $("#editForm select").val(json.employee.id);
                }
                //弹出模态框
                $("#editModal").modal("show");
            });

            //给保存按钮绑定点击事件
            $(".btn-submit").click(function () {
                //当发送异步请求中含有form表单时,可以使用jQuery-form插件进行异步提交
                $("#editForm").ajaxSubmit(messageHandler);
            });

            //删除操作
            $(".btn-delete").click(function () {
                //获取data-id的值
                var id = $(this).data("id");
                //确认框   title标题,需要用户确认的信息,点击确认后的回调函数
                $.messager.confirm("警告:", "您确定要删除这条数据吗?", function () {
                    //messageHandler  是抽取出来的函数 在js/system/commonAll.js中
                    $.post('/classinfo/delete.do', {id: id}, messageHandler)
                });
            });
        });
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <#include "../common/navbar.ftl"/>
    <!-- 菜单回显-->
    <#assign currentMenu="classinfo"/>
    <#include "../common/menu.ftl"/>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>班级管理</h1>
        </section>
        <section class="content">
            <div class="box">
                <!--高级查询--->
                <form class="form-inline" id="searchForm" action="/classinfo/list.do" method="post">
                    <input type="hidden" name="currentPage" id="currentPage" value="1">
                    <div class="form-group">
                        <label for="keyword">关键字:</label>
                        <input type="text" class="form-control" id="keyword" name="keyword" value="${(qo.keyword)!}"
                               placeholder="请输入班级名称">
                    </div>
                    <div class="form-group">
                        <label for="employee"> 班主任:</label>
                        <select class="form-control" id="employee" name="employeeId">
                            <option value="-1">全部</option>
                                <#list employees! as employee>
                                    <option value="${employee.id}">${employee.name}</option>
                                </#list>
                        </select>
                        <script>
                            $("#employee option[value='${(qo.employeeId)!}']").prop("selected", true);
                        </script>
                    </div>
                    <button id="btn_query" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>
                        查询
                    </button>
                    <a href="#" class="btn btn-success btn-input" style="margin: 10px">
                        <span class="glyphicon glyphicon-plus"></span> 添加
                    </a>
                </form>
                <!--编写内容-->
                <div class="box-body table-responsive no-padding ">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th>编号</th>
                            <th>班级名称</th>
                            <th>班级人数</th>
                            <th>班主任</th>
                            <th>操作</th>
                        </tr>
                        <#list (pageInfo.list)! as classinfo>
                            <tr>
                            <#-- 序号列    索引值从0开始,所以需要+1 -->
                                <td>${classinfo_index+1}</td>
                                <td>${classinfo.name}</td>
                                <td>${classinfo.number}</td>
                                <td>${classinfo.employee.name}</td>
                                <td>
                                    <a href="#" class="btn btn-info btn-xs btn-input" data-json='${classinfo.json}'>
                                        <span class="glyphicon glyphicon-pencil"></span> 编辑
                                    </a>
                                    <a href="#" class="btn btn-danger btn-xs btn-delete" data-id="${classinfo.id}">
                                        <span class="glyphicon glyphicon-trash"></span> 删除
                                    </a>
                                </td>
                            </tr>
                        </#list>
                    </table>
                    <!--分页-->
                    <#include "../common/page.ftl"/>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- 模态框 -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">新增/编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/classinfo/saveOrUpdate.do" method="post" id="editForm">
                    <input type="hidden" name="id">
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="name" class="col-sm-3 control-label">班级名称：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="name" name="name"
                                   placeholder="请输入班级名称">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">班级人数：</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="number" name="number"
                                   placeholder="请输入班级人数">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <label for="sn" class="col-sm-3 control-label">班主任：</label>
                        <div class="col-sm-7">
                            <select id="employee_id" name="employee_id" class="form-control">
                                <option value="-1">请选择班主任:</option>
                                <#list employees as employee>
                                    <option value="${employee.id}">${employee.name}</option>
                                </#list>

                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary btn-submit">保存</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
