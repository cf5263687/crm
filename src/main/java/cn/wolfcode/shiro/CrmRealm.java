package cn.wolfcode.shiro;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.domain.Role;
import cn.wolfcode.mapper.EmployeeMapper;
import cn.wolfcode.mapper.PermissionMapper;
import cn.wolfcode.mapper.RoleMapper;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("crmRealm")
public class CrmRealm extends AuthorizingRealm {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    @Override
    public void setCredentialsMatcher(CredentialsMatcher credentialsMatcher) {
        super.setCredentialsMatcher(credentialsMatcher);
    }

    /**
     * 提供认证信息
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
       /* //准备假数据  admin  1
        String username = "admin";
        String password = "1";*/
        //判断token里面的用户名是否存在, 现在是假数据,不是从数据库中查的,那就是判断是否与username相同
        Employee employee = employeeMapper.selectByName((String) token.getPrincipal());

        //getPrincipal()方法其实就是获取token里面的用户名
        //if(username.equals(token.getPrincipal())){
        if(employee != null){
            if(!employee.isStatus()){//账户禁用
                //throw new RuntimeException("账户已被禁用,请联系管理员");
                throw new DisabledAccountException();
            }
                                        //员工对象(自动绑定到subject),   密码,         盐(此时是用户名作为盐),                当前数据源的名字(标记)
            return new SimpleAuthenticationInfo(employee,employee.getPassword(), ByteSource.Util.bytes(employee.getName()),"crmRealm" );
            //如果返回的不为null，shiro就会接着去做密码认证的功能
        }
        return null;//如果返回null，shiro会抛出没有账号的异常
    }

    /**
     * 提供角色和权限信息    授权
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("------开始查询------");
        //该方法返回的结果中包含了哪些角色和权限数据,那么当前的subject主体就拥有哪些角色和权限数据
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
      /*测试用假信息
        info.addRole("HR");//设置角色
        info.addStringPermission("employee:list");//设置权限*/

        //获取当前登录用户的id
        //可以通过安全工具类获取当前登录用户
       /* Subject subject = SecurityUtils.getSubject();
        Employee employee= (Employee) subject.getPrincipal();*/
        //也可以使用参数   shiro知道,查询用户权限信息时,需要用到身份信息,所以形参上就注入进来了
        Employee employee = (Employee) principalCollection.getPrimaryPrincipal();
        if(employee.isAdmin()){//是管理员
            info.addStringPermission("*:*");//通配符   所有权限
            info.addRole("Admin");//管理员的角色
        }else{
            //根据员工id查询该员工拥有的权限  权限表达式
            List<String> expressions = permissionMapper.selectExpressionsByEmpId(employee.getId());
            info.addStringPermissions(expressions);

            //根据员工id查询该员工拥有的角色
            List<String> roles = roleMapper.selectSnByEmpId(employee.getId());
            info.addRoles(roles);
        }
        System.out.println("------结束查询------");
        return info;
    }
}
