package cn.wolfcode.shiro;

import cn.wolfcode.util.JsonResult;
import com.alibaba.fastjson.JSON;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

@Component("ajaxFormAuthenticationFilter")
public class AjaxFormAuthenticationFilter extends FormAuthenticationFilter {
    @Override
    protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request, ServletResponse response) throws Exception {
        //使用response输出json的数据给前端
        JsonResult json = new JsonResult();//默认为true,JSONResult类中指定了默认值
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(JSON.toJSONString(json));
        return false;//不放行,因为我们自己响应JSON数据回去了
    }

    @Override
    protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException e, ServletRequest request, ServletResponse response) {
        //使用response输出json的数据给前端
        String msg = "登陆失败,请联系管理员";
        if (e instanceof UnknownAccountException) {
            msg = "账户不存在";
        }else if(e instanceof IncorrectCredentialsException){
            msg="密码错误";
        }else if(e instanceof DisabledAccountException){
            msg="账户被禁用,请联系管理员";
        }
        JsonResult json = new JsonResult(false, msg);
        response.setContentType("application/json;charset=utf-8");
        try {
            response.getWriter().write(JSON.toJSONString(json));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return false;//不放行
    }

   @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        if(isLoginRequest(request,response)&&isLoginSubmission(request,response)){
            return false;
        }

        return super.isAccessAllowed(request, response, mappedValue);
    }
}
