package cn.wolfcode.excepotion;

import cn.wolfcode.util.JsonResult;
import com.alibaba.fastjson.JSON;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 对控制器进行增强
 */
@ControllerAdvice
public class CRMExceptionHandler {
    /**
     * 指定要捕获的异常
     */
    @ExceptionHandler(Exception.class)
    //@ResponseBody     不能使用贴注解的方式返回JSON字符串,需要手动响应回去
    public Object exceptionHandler(Exception e, HandlerMethod handlerMethod, HttpServletResponse response) throws IOException {//进入该方法,就表示捕获到了异常
        //把异常信息打印出,方便我们开发的时候找bug
        e.printStackTrace();
        if(handlerMethod.hasMethodAnnotation(ResponseBody.class)){//方法上贴了@ResponseBody注解
            //如果是发送AJAX请求出现的异常,那么异常返回JSON数据(贴了@ResponseBody注解的方法)
            JsonResult json = new JsonResult(false,"系统异常,请联系管理员");
            //修改响应页面格式和编码格式
            response.setContentType("application/json;charset=utf-8");
            //使用response对象输出数据,把json字符串响应回去
            response.getWriter().write(JSON.toJSONString(json));
            return null;
        }else{//访问错误视图页面
            return "common/error";//配置了freemarker试图解析器  会加上前缀后缀
        }

    }

    @ExceptionHandler(UnauthorizedException.class)
    public Object authorizedExceptionHandler(Exception e, HandlerMethod handlerMethod, HttpServletResponse response) throws IOException {//进入该方法,就表示捕获到了异常
        //把异常信息打印出,方便我们开发的时候找bug
        e.printStackTrace();
        if(handlerMethod.hasMethodAnnotation(ResponseBody.class)){
            JsonResult json = new JsonResult(false,"您无权访问此内容!");
            //修改响应页面格式和编码格式
            response.setContentType("application/json;charset=utf-8");
            //使用response对象输出数据,把json字符串响应回去
            response.getWriter().write(JSON.toJSONString(json));
            return null;
        }else{//访问无权访问视图页面
            return "common/nopermission";//配置了freemarker试图解析器  会加上前缀后缀
        }
    }
	
	
    /*@ExceptionHandler(NullPointException.class)
    public void exceptionHandler1(){

    }
    @ExceptionHandler(NumberFormatException.class)
    public void exceptionHandler2(){

    }*/
}
