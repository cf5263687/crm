package cn.wolfcode.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 角色
 */
@Getter
@Setter
public class Role {
    private Long id;

    private String name;

    private String sn;

    private List<Permission> permissions;

}