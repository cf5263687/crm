package cn.wolfcode.domain;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * 字典目录
 */
@Getter
@Setter
public class SystemDictionary {
    private Long id;

    private String sn;  //编码

    private String title;   //标题

    private String intro;   //介绍

    public String getJson(){
        //新建一个map集合,页面需要什么数据就传入什么数据
        Map<String,Object> map = new HashMap<>();
        map.put("id",id);
        map.put("sn",sn);
        map.put("title",title);
        map.put("intro",intro);

        //转成JSON字符串,并返回
        return JSON.toJSONString(map);
    }

}