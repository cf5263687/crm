package cn.wolfcode.domain;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Department {
    private Long id;
    private String name;
    private String sn;

    public String getJson(){
        //新建一个map集合,页面需要什么数据就传入什么数据
        Map<String,Object> map = new HashMap<>();
        map.put("id",id);
        map.put("name",name);
        map.put("sn",sn);

        //转成JSON字符串,并返回
        return JSON.toJSONString(map);
    }
}
