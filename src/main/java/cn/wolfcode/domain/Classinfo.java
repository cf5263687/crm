package cn.wolfcode.domain;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Classinfo {
    private Long id;
    private String name;        //班级名称
    private Integer number;     //班级人数
    private Long employee_id;   //班主任id

    private Employee employee;  //员工对象(班主任)

    public String getJson(){
        Map<String,Object> map = new HashMap<>();
        map.put("id",id);
        map.put("name",name);
        map.put("number",number);
        map.put("employee",employee);

        //返回JSON字符串
        return JSON.toJSONString(map);
    }



}