package cn.wolfcode.domain;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@ToString
public class CourseOrder {
    private Long id;
    private Date inputTime;     //录入时间
    private Customer customer;    //用户
    private SystemDictionaryItem course;      //课程
    private Long money;         //销售金额

    public String getJson(){
        Map<String,Object> map = new HashMap<>();
        map.put("id",id);
        map.put("customer",customer);
        map.put("course",course);
        map.put("money",money);
        return JSON.toJSONString(map);
    }
}