package cn.wolfcode.domain;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Employee {
    private Long id;
    private String name;
    private String password;
    private String email;
    private Integer age;
    private boolean admin; //用基本类型  不用包装类型  包装类型中含有null
    private Department dept; //不仅可以装deptId,也可以装name
    private List<Role> roles = new ArrayList<>(); //角色集合
    private boolean status;//状态  1为正常  0为禁用
}