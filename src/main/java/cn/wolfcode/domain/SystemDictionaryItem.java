package cn.wolfcode.domain;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * 字典明细
 */
@Setter
@Getter
public class SystemDictionaryItem {
    private Long id;

    private Long parentId;  //字典目录id

    private String title;   //标题

    private Integer sequence;   //排列序号    可以调整显示的先后顺序

    public String getJson(){
        //新建一个map集合,页面需要什么数据就传入什么数据
        Map<String,Object> map = new HashMap<>();
        map.put("id",id);
        map.put("parentId",parentId);
        map.put("title",title);
        map.put("sequence",sequence);

        //转成JSON字符串,并返回
        return JSON.toJSONString(map);
    }

}