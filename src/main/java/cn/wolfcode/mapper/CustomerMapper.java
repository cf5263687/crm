package cn.wolfcode.mapper;

import cn.wolfcode.domain.Customer;
import cn.wolfcode.qo.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Customer record);

    Customer selectByPrimaryKey(Long id);

    List<Customer> selectAll();

    int updateByPrimaryKey(Customer record);

    List<Customer> selectForList(QueryObject qo);

    Customer selectByTel(String tel);

    void updateStatusById(Long id);

    void updateSeller(@Param("customerId") Long customerId, @Param("sellerId") Long sellerId);
}