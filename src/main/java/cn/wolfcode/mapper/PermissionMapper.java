package cn.wolfcode.mapper;

import cn.wolfcode.domain.Permission;
import cn.wolfcode.qo.QueryObject;
import java.util.List;

public interface PermissionMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Permission record);

    Permission selectByPrimaryKey(Long id);

    List<Permission> selectAll();

    int updateByPrimaryKey(Permission record);


    List<Permission> selectForList(QueryObject qo);

    List<String> selectAllExpression();

    /**
     * 根据员工id查询员工拥有的权限
     * @param id    员工id
     * @return  员工拥有的权限列表
     */
    List<String> selectExpressionsByEmpId(Long id);
}