package cn.wolfcode.mapper;

import cn.wolfcode.domain.Department;
import cn.wolfcode.qo.QueryObject;

import java.util.List;

public interface DepartmentMapper {

    void insert(Department dept);
    void delete(Long id);
    void update(Department dept);
    Department selectOne(Long id);
    List<Department> selectAll();

    //查询当前页的结果
    List<Department> selectForList(QueryObject qo);
}
