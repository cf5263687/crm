package cn.wolfcode.mapper;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.qo.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Employee record);

    Employee selectByPrimaryKey(Long id);

    List<Employee> selectAll();

    int updateByPrimaryKey(Employee record);

    List<Employee> selectForList(QueryObject qo);

    /**
     * 添加员工角色中间表信息的方法
     * @param eid   员工id
     * @param rid   角色id
     */
    void insertRelation(@Param("eid") Long eid, @Param("rid") Long rid);

    /**
     * 使用批量添加的方式    添加员工角色中间表信息
     * @param eid    员工id
     * @param ids   角色id
     */
    void insertRelation2(@Param("eid") Long eid, @Param("ids") Long[] ids);

    void deleteRelation(Long id);

    Employee selectByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    void updatePwdById(@Param("id") Long id, @Param("newPassword") String newPassword);

    void batchDelete(Long[] ids);

    Employee selectByName(String name);

    void updateStatus(Long id);

    List<Employee> selectByClassinfo();

    List<Employee> selectByRoleSn(String ...sns);
}