package cn.wolfcode.util;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.domain.Permission;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;

public abstract class UserContext {

    public static HttpSession getSession(){
        //spring mvc 提供的,获取请求相关的属性 工具类
        //可以在任意地方获取Request  response  session
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        // 是一个接口  所以需要强转一下  转成他的实现类
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) requestAttributes;
        //获取request请求对象,然后获取session对象
        HttpSession session = servletRequestAttributes.getRequest().getSession();
        return session;
    }
    /**
     * 设置当前登录用户到session中
     * @param employee
     */
    public static void setCurrentUser(Employee employee){
        //设置当前登录用户到session中
        getSession().setAttribute("EMPLOYEE_IN_SESSION",employee);
    }

    /**
     * 从session中获取当前登录用户
     * @return  当前登录用户
     */
    public static Employee getCurrentUser(){
        Employee employee = (Employee) getSession().getAttribute("EMPLOYEE_IN_SESSION");
        return employee;
    }

    /**
     * 设置当前用户的所有权限表达式到session中
     * @param expressions   权限表达式集合
     */
    public static void setPermissionExpressions(List<String> expressions){
        getSession().setAttribute("EXPRESSIONS_IN_SESSION",expressions);
    }

    /**
     * 从session中获取所有权限表达式
     * @return  权限表达式集合
     */
    public static List<String> getPermissionExpressions(){
        List<String> expressions = (List<String>) getSession().getAttribute("EXPRESSIONS_IN_SESSION");
        return expressions;
    }
}
