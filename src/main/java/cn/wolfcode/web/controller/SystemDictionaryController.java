package cn.wolfcode.web.controller;

import cn.wolfcode.domain.SystemDictionary;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.service.ISystemDictionaryService;
import cn.wolfcode.util.JsonResult;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/systemDictionary")
public class SystemDictionaryController {

    @Autowired
    private ISystemDictionaryService systemDictionaryService;

    //查询字典目录分页数据
    //@RequiredPermission(value = "字典目录列表", expression = "systemDictionary:list")
    @RequiresPermissions(value={"systemDictionary:list","字典目录列表"},logical = Logical.OR)
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo") QueryObject qo) {

        PageInfo<SystemDictionary> pageInfo = systemDictionaryService.query(qo);
        model.addAttribute("pageInfo", pageInfo);
        return "systemDictionary/list";   //配置了前缀后缀  /WEB-INF/views/systemDictionary/potentialList.ftl
    }

    //删除数据操作
    //@RequiredPermission(value = "字典目录删除", expression = "systemDictionary:delete")
    @RequiresPermissions(value={"systemDictionary:delete","字典目录删除"},logical = Logical.OR)
    @RequestMapping("/delete")
    @ResponseBody
    public JsonResult delete(Long id) {

        systemDictionaryService.delete(id);
        return new JsonResult();

    }

    //保存或修改操作
    //@RequiredPermission(value = "字典目录新增/编辑", expression = "systemDictionary:saveOrUpdate")
    @RequiresPermissions(value={"systemDictionary:saveOrUpdate","字典目录添加/更新"},logical = Logical.OR)
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public JsonResult saveOrUpdate(SystemDictionary systemDictionary) {
        //int i = 1/0;
        if (systemDictionary.getId() != null) {
            //进行修改操作
            systemDictionaryService.update(systemDictionary);
        } else {
            //进行保存操作
            systemDictionaryService.save(systemDictionary);
        }
        return new JsonResult();

    }
}
