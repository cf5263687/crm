package cn.wolfcode.web.controller;

import cn.wolfcode.domain.Classinfo;
import cn.wolfcode.domain.Employee;
import cn.wolfcode.qo.ClassinfoQueryObject;
import cn.wolfcode.service.impl.ClassinfoServiceImpl;
import cn.wolfcode.service.impl.EmployeeServiceImpl;
import cn.wolfcode.util.JsonResult;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping("/classinfo")
@Controller
public class ClassinfoController {

    @Autowired
    private ClassinfoServiceImpl classinfoService;
    @Autowired
    private EmployeeServiceImpl employeeService;

    @RequestMapping("/list")
    public String list(Model model,@ModelAttribute("qo")ClassinfoQueryObject qo){
        PageInfo pageInfo = classinfoService.query(qo);
        model.addAttribute("pageInfo",pageInfo);
        //查询所有是班主任的的员工对象,并保存到模型中
        List<Employee> employees = employeeService.selectByClassinfo();
        model.addAttribute("employees",employees);
        return "/classinfo/list";
    }

    @RequestMapping("/delete")
    @ResponseBody
    public JsonResult delete(Long id ){
        classinfoService.delete(id);
        return new JsonResult();
    }

    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public JsonResult saveOrUpdate(Classinfo classinfo){
        if(classinfo.getId() == null){
            //新增操作
            classinfoService.save(classinfo);
        }else{
            //修改操作
            classinfoService.update(classinfo);
        }
        return new JsonResult();
    }
}
