package cn.wolfcode.web.controller;

import cn.wolfcode.domain.Permission;
import cn.wolfcode.util.JsonResult;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.service.IPermissionService;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    private IPermissionService permissionService;

    //查询权限分页数据
    //@RequiredPermission(value="权限列表",expression="permission:list")
    @RequiresPermissions(value={"permission:list","权限列表"},logical = Logical.OR)
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo")QueryObject qo){
        PageInfo<Permission> pageInfo = permissionService.query(qo);
        model.addAttribute("pageInfo",pageInfo);
        return "permission/list";   //配置了前缀后缀  /WEB-INF/views/permission/potentialList.ftl
    }

    //删除数据操作
    //@RequiredPermission(value="权限删除",expression="permission:delete")
    @RequiresPermissions(value={"permission:delete","权限删除"},logical = Logical.OR)
    @RequestMapping("/delete")
    public String delete(Long id){
        if(id != null){
            permissionService.delete(id);
        }
        return "redirect:/permission/list.do";
    }

    @RequiresPermissions(value={"permission:reload","权限加载"},logical = Logical.OR)
    @RequestMapping("/reload")
    @ResponseBody
    public JsonResult reload(){
        try {
            permissionService.reload(); //执行业务
            return new JsonResult();    //默认成功  在JSONResult类中设置了
        }catch (Exception e){
            e.printStackTrace();
            return new JsonResult(false, "加载失败,请联系管理员"); //失败
        }

    }

}
