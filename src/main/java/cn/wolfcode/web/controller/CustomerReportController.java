package cn.wolfcode.web.controller;

import cn.wolfcode.qo.CustomerReportQueryObject;
import cn.wolfcode.service.ICustomerReportService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/customerReport")
public class CustomerReportController {
    @Autowired
    private ICustomerReportService customerReportService;
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo")CustomerReportQueryObject qo){
        PageInfo pageInfo = customerReportService.query(qo);
        model.addAttribute("pageInfo",pageInfo);
        return "/customerReport/list";
    }

    //柱状图页面
    @RequestMapping("/listByBar")
    public String listByBar(Model model, @ModelAttribute("qo")CustomerReportQueryObject qo){
        //查询报表相关的数据(不做分页)
        //list中存放的时一个个的map集合  每个map有两组数据    key是分组方式 value是具体的分组方式
                                                    //key是潜在客户数 value是具体的数量
        List<Map> list = customerReportService.selectAll(qo);
        //提供报表需要的数据     x轴数据和y轴数据   提供两个集合
        List<Object> xList = new ArrayList<>();
        List<Object> yList = new ArrayList<>();
        //遍历list集合,把每一条map集合存到xList和yList中
        for (Map map : list) {
            xList.add(map.get("groupType"));
            yList.add(map.get("number"));
        }
        //把数据共享出去   freemarker不能显示非字符串的内容(日期,集合,布尔)   所以把集合转换成Json字符串,
        model.addAttribute("xList", JSON.toJSONString(xList));
        model.addAttribute("yList",JSON.toJSONString(yList));
        return "/customerReport/listByBar";
    }
    //饼图页面
    @RequestMapping("/listByPie")
    public String listByPie(Model model, @ModelAttribute("qo")CustomerReportQueryObject qo){
        //查询报表相关的数据(不做分页)
        //list中存放的时一个个的map集合  每个map有两组数据    key是分组方式 value是具体的分组方式
                                                    //key是潜在客户数 value是具体的数量
        List<Map> list = customerReportService.selectAll(qo);
        //提供报表需要的数据  提供两个集合
        List<Object> legendList = new ArrayList<>();
        List<Object> seriesList = new ArrayList<>();
        //遍历list集合,把每一条map集合存到xList和yList中
        for (Map map : list) {
            legendList.add(map.get("groupType"));
            Map temp = new HashMap();
            temp.put("value",map.get("number"));
            temp.put("name",map.get("groupType"));
            seriesList.add(temp);
        }
        //把数据共享出去   freemarker不能显示非字符串的内容(日期,集合,布尔)   所以把集合转换成Json字符串,
        model.addAttribute("legendList", JSON.toJSONString(legendList));
        model.addAttribute("seriesList",JSON.toJSONString(seriesList));
        return "/customerReport/listByPie";
    }
}
