package cn.wolfcode.web.controller;

import cn.wolfcode.domain.Department;
import cn.wolfcode.domain.Employee;
import cn.wolfcode.domain.Role;
import cn.wolfcode.qo.EmployeeQueryObject;
import cn.wolfcode.service.IDepartmentService;
import cn.wolfcode.service.IEmployeeService;
import cn.wolfcode.service.IRoleService;
import cn.wolfcode.util.JsonResult;
import com.github.pagehelper.PageInfo;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;
    @Autowired
    private IDepartmentService departmentService;
    @Autowired
    private IRoleService roleService;

    //查询员工,分页数据,模糊查找
    @RequestMapping("/list")
    //@RequiredPermission(value = "员工列表", expression = "employee:list")
    @RequiresPermissions(value={"employee:list","员工列表"},logical = Logical.OR)
    public String list(Model model, @ModelAttribute("qo") EmployeeQueryObject qo) {
        //int i = 1/0;
//        Subject subject = SecurityUtils.getSubject();
//        System.out.println("判断用户是否拥有HR的角色"+subject.hasRole("HR_MGR"));
//        System.out.println("判断用户是否拥有MANAGER的角色"+subject.hasRole("ORDER_MGR"));
//        System.out.println("判断用户是否拥有employee:list的权限"+subject.isPermitted("employee:list"));

        PageInfo<Employee> pageInfo = employeeService.query(qo);
        model.addAttribute("pageInfo", pageInfo);
        //查询所有部门数据,插到模型中
        List<Department> departments = departmentService.listAll();
        model.addAttribute("departments", departments);
        return "employee/list";   //配置了前缀后缀  /WEB-INF/views/employee/potentialList.ftl
    }

    //删除数据操作
    //@RequiredPermission(value = "员工删除", expression = "employee:delete")
    @RequiresPermissions(value={"employee:delete","员工删除"},logical = Logical.OR)
    @RequestMapping("/delete")
    public String delete(Long id) {
        if (id != null) {
            employeeService.delete(id);
        }
        return "redirect:/employee/list.do";
    }

    //@RequiredPermission(value = "批量员工删除", expression = "employee:batchDelete")
    @RequiresPermissions(value={"employee:batchDelete","员工批量删除"},logical = Logical.OR)
    @RequestMapping("/batchDelete")
    @ResponseBody
    public JsonResult batchDelete(Long[] ids) {

        employeeService.batchDelete(ids);
        return new JsonResult();

    }

    //新增或修改的页面
    //@RequiredPermission(value = "员工编辑页面", expression = "employee:input")
    @RequiresPermissions(value={"employee:input","员工编辑"},logical = Logical.OR)
    @RequestMapping("/input")
    public String input(Model model, Long id) {
        //获取所有的角色
        List<Role> roles = roleService.listAll();
        model.addAttribute("roles", roles);
        //获取所有的部门
        List<Department> departments = departmentService.listAll();
        model.addAttribute("departments", departments);
        if (id != null) {
            model.addAttribute("employee", employeeService.get(id));
        }
        return "employee/input";  //  /WEB-INF/views/employee/input.ftl
    }

    //保存或修改操作
    //@RequiredPermission(value = "员工新增/编辑", expression = "employee:saveOrUpdate")
    @RequiresPermissions(value={"employee:saveOrUpdate","员工添加/更新"},logical = Logical.OR)
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public JsonResult saveOrUpdate(Employee employee, Long[] ids) {

        if (employee.getId() != null) {
            //进行修改操作
            employeeService.update(employee, ids);
        } else {
            //进行保存操作
            employeeService.save(employee, ids);
        }
        return new JsonResult();


    }

    @RequestMapping("/inputPwd")
    public String inputPwd(Model model, Long id, String name) {
        if (id != null) {
            model.addAttribute("id", id);
            model.addAttribute("name", name);
            return "employee/resetPwd";
        }
        return "employee/updatePwd";
    }

    @RequestMapping("/updatePwd")
    @ResponseBody
    public JsonResult updatePwd(String oldPassword, String newPassword, HttpSession session) {
        if (newPassword != null && newPassword.trim().length() > 0 && oldPassword != null && oldPassword.trim().length() > 0) {
            if (!newPassword.equals(oldPassword)) {
                //从session中获取当前登录用户
                //Employee employee = (Employee) session.getAttribute("EMPLOYEE_IN_SESSION");
                //Employee employee = UserContext.getCurrentUser();
                Subject subject = SecurityUtils.getSubject();
                Employee employee = (Employee) subject.getPrincipal();
                //将用户输入的密码进行加密处理
                oldPassword = new Md5Hash(oldPassword,employee.getName()).toString();
                newPassword = new Md5Hash(newPassword,employee.getName()).toString();
                if (employee.getPassword().equals(oldPassword)) {
                    employeeService.updatePwd(employee.getId(), newPassword);
                    //销毁session
                    //session.invalidate();
                    return new JsonResult(true, "密码修改成功");
                }
                return new JsonResult(false, "原密码错误,修改密码失败");
            }
            return new JsonResult(false, "新旧密码不能相同");
        }
        return new JsonResult(false, "密码不能为空");
    }

    @RequestMapping("/resetPwd")
    @ResponseBody
    public JsonResult resetPwd(Long id, String name, String newPassword) {
        if (newPassword != null) {
            //给新密码进行加密然后存入数据库中
            newPassword = new Md5Hash(newPassword,name).toString();
            employeeService.updatePwd(id, newPassword);
            return new JsonResult(true, "密码重置成功");
        }
        return new JsonResult(false, "密码不能为空");
    }

    @RequestMapping("/updateStatus")
    @ResponseBody
    public JsonResult updateStatus(Long id) {
        employeeService.updateStatus(id);
        return new JsonResult();
    }

    @RequestMapping("/checkName")
    @ResponseBody
    public Map<String, Boolean> checkName(String name, Long id) {
        Map<String, Boolean> map = new HashMap<>();
        if (id != null) {
            Employee employee = employeeService.get(id);
            if (name.equals(employee.getName())) {
                map.put("valid", true);
                return map;
            }
        }
        Employee employee = employeeService.selectByName(name);
        //employee为空    值则为true     验证则通过
        //employee不为空   值为false     验证失败
        map.put("valid", employee == null);
        return map;
    }

    @RequestMapping("/exportXls")
    public void exportXls(HttpServletResponse response) throws IOException {
        //文件下载的响应头（让浏览器访问资源的的时候以下载的方式打开）
        response.setHeader("Content-Disposition","attachment;filename=employee.xls");
        //创建工作簿
        Workbook workbook = employeeService.exportXls();
        //将文件输出到浏览器
        workbook.write(response.getOutputStream());
    }

    @RequestMapping("/importXls")
    @ResponseBody
    public JsonResult importXls(MultipartFile file) throws IOException {
        //System.out.println(file);
        employeeService.importXls(file);
        return new JsonResult();
    }
}
