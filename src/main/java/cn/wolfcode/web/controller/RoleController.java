package cn.wolfcode.web.controller;

import cn.wolfcode.domain.Department;
import cn.wolfcode.domain.Permission;
import cn.wolfcode.domain.Role;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.qo.ResultPage;
import cn.wolfcode.service.IDepartmentService;
import cn.wolfcode.service.IPermissionService;
import cn.wolfcode.service.IRoleService;
import cn.wolfcode.util.RequiredPermission;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private IRoleService roleService;
    @Autowired
    private IPermissionService permissionService;

    //查询角色,分页数据
    //@RequiredPermission(value="角色列表",expression="role:list")
    @RequiresPermissions(value={"role:list","角色列表"},logical = Logical.OR)
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo")QueryObject qo){
        //查询所有角色数据,插到模型中
        PageInfo<Role> pageInfo = roleService.query(qo);
        model.addAttribute("pageInfo",pageInfo);
        return "role/list";   //配置了前缀后缀  /WEB-INF/views/role/potentialList.ftl
    }

    //删除数据操作
    //@RequiredPermission(value="角色删除",expression="role:delete")
    @RequiresPermissions(value={"role:delete","角色删除"},logical = Logical.OR)
    @RequestMapping("/delete")
    public String delete(Long id){
        if(id != null){
            roleService.delete(id);
        }
        return "redirect:/role/list.do";
    }

    //新增或修改的页面
    //@RequiredPermission(value="角色编辑页面",expression="role:input")
    @RequiresPermissions(value={"role:input","角色编辑"},logical = Logical.OR)
    @RequestMapping("/input")
    public String input(Model model,Long id){
        //查询所有权限数据,插到模型中
        List<Permission> permissions = permissionService.listAll();
        //查询所有权限
        model.addAttribute("permissions",permissions);
        if(id != null){
            model.addAttribute("role",roleService.get(id));
        }
        return "role/input";  //  /WEB-INF/views/role/input.ftl
    }

    //保存或修改操作
    //@RequiredPermission(value="角色新增/编辑",expression="role:saveOrUpdate")
    @RequiresPermissions(value={"role:saveOrUpdate","角色添加/更新"},logical = Logical.OR)
    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(Role role,Long[] ids){
        if(role.getId() != null){
            //进行修改操作
            roleService.update(role,ids);
        }else{
            //进行保存操作
            roleService.save(role,ids);
        }
        return "redirect:/role/list.do";
    }
}
