package cn.wolfcode.web.controller;

import cn.wolfcode.domain.Department;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.service.IDepartmentService;
import cn.wolfcode.util.JsonResult;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private IDepartmentService departmentService;

    //查询部门分页数据
    //@RequiredPermission(value = "部门列表", expression = "department:list")
    @RequiresPermissions(value={"department:list","部门列表"},logical = Logical.OR)
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo") QueryObject qo) {
        PageInfo<Department> pageInfo = departmentService.query(qo);
        model.addAttribute("pageInfo", pageInfo);
        return "department/list";   //配置了前缀后缀  /WEB-INF/views/department/potentialList.ftl
    }

    //删除数据操作
    //@RequiredPermission(value = "部门删除", expression = "department:delete")
    @RequiresPermissions(value={"department:delete","部门删除"},logical = Logical.OR)
    @RequestMapping("/delete")
    @ResponseBody
    public JsonResult delete(Long id) {

        departmentService.delete(id);
        return new JsonResult();

    }

    //保存或修改操作
    //@RequiredPermission(value = "部门新增/编辑", expression = "department:saveOrUpdate")
    @RequiresPermissions(value={"department:saveOrUpdate","部门添加/更新"},logical = Logical.OR)
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public JsonResult saveOrUpdate(Department department) {
        //int i = 1/0;
        if (department.getId() != null) {
            //进行修改操作
            departmentService.update(department);
        } else {
            //进行保存操作
            departmentService.save(department);
        }
        return new JsonResult();

    }
}
