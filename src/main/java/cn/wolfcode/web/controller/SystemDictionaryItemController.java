package cn.wolfcode.web.controller;

import cn.wolfcode.domain.SystemDictionary;
import cn.wolfcode.domain.SystemDictionaryItem;
import cn.wolfcode.qo.SystemDictionaryItemQueryObject;
import cn.wolfcode.service.ISystemDictionaryItemService;
import cn.wolfcode.service.ISystemDictionaryService;
import cn.wolfcode.util.JsonResult;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/systemDictionaryItem")
public class SystemDictionaryItemController {

    @Autowired
    private ISystemDictionaryItemService systemDictionaryItemService;

    @Autowired
    private ISystemDictionaryService systemDictionaryService;
    //查询字典明细分页数据
    //@RequiredPermission(value = "字典明细列表", expression = "systemDictionaryItem:list")
    @RequiresPermissions(value={"systemDictionaryItem:list","字典明细列表"},logical = Logical.OR)
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo")SystemDictionaryItemQueryObject qo) {
        PageInfo<SystemDictionaryItem> pageInfo = null;
        if(qo.getParentId() != -1L){
            pageInfo = systemDictionaryItemService.query(qo);
        }
        model.addAttribute("pageInfo", pageInfo);
        //查询所有的字典目录
        List<SystemDictionary> systemDictionaries = systemDictionaryService.listAll();
        model.addAttribute("systemDictionaries",systemDictionaries);
        return "systemDictionaryItem/list";   //配置了前缀后缀  /WEB-INF/views/systemDictionaryItem/potentialList.ftl
    }

    //删除数据操作
    //@RequiredPermission(value = "字典明细删除", expression = "systemDictionaryItem:delete")
    @RequiresPermissions(value={"systemDictionaryItem:delete","字典明细删除"},logical = Logical.OR)
    @RequestMapping("/delete")
    @ResponseBody
    public JsonResult delete(Long id) {

        systemDictionaryItemService.delete(id);
        return new JsonResult();

    }

    //保存或修改操作
    //@RequiredPermission(value = "字典明细新增/编辑", expression = "systemDictionaryItem:saveOrUpdate")
    @RequiresPermissions(value={"systemDictionaryItem:saveOrUpdate","字典明细添加/更新"},logical = Logical.OR)
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public JsonResult saveOrUpdate(SystemDictionaryItem systemDictionaryItem) {
        //int i = 1/0;
        //点击新增或修改时序号没有填写的话,默认设置为最大序号+1
        if(systemDictionaryItem.getSequence() == null){
           int sequence =  systemDictionaryItemService.selectSequenceByParentId(systemDictionaryItem.getParentId());
           systemDictionaryItem.setSequence(sequence + 1);
        }
        if (systemDictionaryItem.getId() != null) {
            //进行修改操作
            systemDictionaryItemService.update(systemDictionaryItem);
        } else {
            //进行保存操作
            systemDictionaryItemService.save(systemDictionaryItem);
        }
        return new JsonResult();

    }
}
