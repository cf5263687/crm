package cn.wolfcode.web.controller;


import cn.wolfcode.domain.Customer;
import cn.wolfcode.domain.Employee;
import cn.wolfcode.domain.SystemDictionaryItem;
import cn.wolfcode.qo.CustomerQueryObject;
import cn.wolfcode.service.ICustomerService;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.service.IEmployeeService;
import cn.wolfcode.service.ISystemDictionaryItemService;
import cn.wolfcode.util.JsonResult;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private ICustomerService customerService;
    @Autowired
    private IEmployeeService employeeService;
    @Autowired
    private ISystemDictionaryItemService systemDictionaryItemService;

    @RequiresPermissions(value = {"customer:potentialList","潜在客户页面"},logical = Logical.OR)
    @RequestMapping("/potentialList")
    public String potentialList(Model model, @ModelAttribute("qo") CustomerQueryObject qo){
        //潜在客户页面只查询潜在客户的数据
        qo.setStatus(Customer.STATUS_COMMON);
        //管理员和经理进来潜在客户页面可以查看所有的客户数据,普通销售只能查看自己的客户
        Subject subject = SecurityUtils.getSubject();
        //判断是不是管理员或经理  然后取反
        if(!(subject.hasRole("Admin")||subject.hasRole("Market_Manager"))){
            //获取当前登录的用户
            Employee employee = (Employee) subject.getPrincipal();
            //根据当前登录用户的id查询所跟进的潜在客户
            qo.setSellerId(employee.getId());
        }
        PageInfo<Customer> pageInfo = customerService.query(qo);
        model.addAttribute("pageInfo", pageInfo);
        //查询所有的销售人员   根据角色编码查询拥有该角色的员工 Market市场专员 Market_Manager市场经理
        List<Employee> sellers =  employeeService.selectByRoleSn("Market","Market_Manager");//使用可变参数
        model.addAttribute("sellers",sellers);
        //职业下拉框数据
        List<SystemDictionaryItem> jobs = systemDictionaryItemService.selectBySn("job");
        model.addAttribute("jobs",jobs);
        List<SystemDictionaryItem> sources = systemDictionaryItemService.selectBySn("source");
        model.addAttribute("sources",sources);
        //查询跟进方式
        List<SystemDictionaryItem> ccts = systemDictionaryItemService.selectBySn("communicationMethod");
        model.addAttribute("ccts",ccts);

        return "customer/potentialList";
    }

    @RequestMapping("/delete")
    @RequiresPermissions(value = {"customer:delete","客户删除"},logical = Logical.OR)
    @ResponseBody
    public JsonResult delete(Long id){
        if (id != null) {
            customerService.delete(id);
        }
        return new JsonResult();
    }


    @RequestMapping("/saveOrUpdate")
    @RequiresPermissions(value = {"customer:saveOrUpdate","客户新增/编辑"},logical = Logical.OR)
    @ResponseBody
    public JsonResult saveOrUpdate(Customer customer){
        if (customer.getId() != null) {
            customerService.update(customer);
        }else {
            customerService.save(customer);
        }
        return new JsonResult();
    }

    @RequestMapping("/checkTel")
    @ResponseBody
    public Map<String, Boolean> checkTel(String tel, Long id) {
        Map<String, Boolean> map = new HashMap<>();
        if (id != null) {
            Customer customer = customerService.get(id);
            if (tel.equals(customer.getTel())) {
                map.put("valid", true);
                return map;
            }
        }
        Customer customer = customerService.selectByTel(tel);
        map.put("valid", customer == null);
        return map;
    }
}
