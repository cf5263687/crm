package cn.wolfcode.web.controller;


import cn.wolfcode.domain.CourseOrder;
import cn.wolfcode.domain.Customer;
import cn.wolfcode.domain.SystemDictionaryItem;
import cn.wolfcode.qo.CourseOrderQueryObject;
import cn.wolfcode.service.ICourseOrderService;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.service.ICustomerService;
import cn.wolfcode.service.ISystemDictionaryItemService;
import cn.wolfcode.util.JsonResult;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/courseOrder")
public class CourseOrderController {

    @Autowired
    private ICourseOrderService courseOrderService;
    @Autowired
    private ICustomerService customerService;
    @Autowired
    private ISystemDictionaryItemService systemDictionaryItemService;

    @RequiresPermissions(value = {"courseOrder:list","订单管理页面"},logical = Logical.OR)
    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo")CourseOrderQueryObject qo){
        //查询所有的客户
        List<Customer> customers = customerService.listAll();
        model.addAttribute("customers",customers);
        //查询所有的课程
        List<SystemDictionaryItem> courses = systemDictionaryItemService.selectBySn("subject");
        model.addAttribute("courses",courses);
        PageInfo<CourseOrder> pageInfo = courseOrderService.query(qo);
        System.out.println(qo.getKeyword());
        model.addAttribute("pageInfo", pageInfo);
        return "courseOrder/list";
    }

    @RequestMapping("/saveOrUpdate")
    @RequiresPermissions(value = {"courseOrder:saveOrUpdate","订单管理新增/编辑"},logical = Logical.OR)
    @ResponseBody
    public JsonResult saveOrUpdate(CourseOrder courseOrder){
        if (courseOrder.getId() != null) {
            courseOrderService.update(courseOrder);
        }else {
            courseOrderService.save(courseOrder);
        }
        return new JsonResult();
    }
}
