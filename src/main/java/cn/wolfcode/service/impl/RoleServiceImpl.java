package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Role;
import cn.wolfcode.mapper.PermissionMapper;
import cn.wolfcode.mapper.RoleMapper;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.qo.ResultPage;
import cn.wolfcode.service.IRoleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
@Service
public class RoleServiceImpl implements IRoleService {
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public void save(Role role, Long[] ids) {
        //增加角色
        roleMapper.insert(role);
        //增加角色,权限中间表数据
        // 必须先增加角色,在添加关系,因为添加时id值为空,角色添加成功后id值才存在
        if(ids.length>0&& ids != null){
            for (Long pid:ids) {
                roleMapper.insertRelation(role.getId(),pid);
            }
        }
    }

    @Override
    public void delete(Long id) {
        roleMapper.deleteByPrimaryKey(id);
        //删除角色时同时删除中间表中无用数据
        roleMapper.deleteRelation(id);
    }

    @Override
    public void update(Role role,Long[] ids) {
        //修改角色数据        // 修改时的顺序无所谓,也可以先修改中间表,因为此时id值已固定了
        roleMapper.updateByPrimaryKey(role);
        //先把中间表的数据删除掉,防止重复数据产生
        roleMapper.deleteRelation(role.getId());
        //在中间表建立新的角色,权限关系
        if(ids != null && ids.length>0){
            for (Long pid:ids) {
                roleMapper.insertRelation(role.getId(),pid);
            }
        }
    }

    @Override
    public Role get(Long id) {
        Role role = roleMapper.selectByPrimaryKey(id);
        return role;
    }

    @Override
    public List<Role> listAll() {
        List<Role> roles = roleMapper.selectAll();
        return roles;
    }

    @Override
    public PageInfo query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Role> roles = roleMapper.selectForList(qo);
        return new PageInfo(roles);
    }
}
