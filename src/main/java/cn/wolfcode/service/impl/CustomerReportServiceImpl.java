package cn.wolfcode.service.impl;

import cn.wolfcode.mapper.CustomerReportMapper;
import cn.wolfcode.qo.CustomerReportQueryObject;
import cn.wolfcode.service.ICustomerReportService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class CustomerReportServiceImpl implements ICustomerReportService {

    @Autowired
    private CustomerReportMapper customerReportMapper;

    @Override
    public PageInfo query(CustomerReportQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Map> maps = customerReportMapper.selectCustomerReport(qo);
        return new PageInfo(maps);
    }

    @Override
    public List<Map> selectAll(CustomerReportQueryObject qo) {
        List<Map> maps = customerReportMapper.selectCustomerReport(qo);
        return maps;
    }
}
