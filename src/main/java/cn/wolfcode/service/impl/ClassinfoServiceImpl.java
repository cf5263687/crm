package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Classinfo;
import cn.wolfcode.mapper.ClassinfoMapper;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.service.IClassinfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ClassinfoServiceImpl implements IClassinfoService {

    @Autowired
    private ClassinfoMapper classinfoMapper;

    @Override
    public void save(Classinfo classinfo) {
        classinfoMapper.insert(classinfo);
    }

    @Override
    public void delete(Long id) {
        classinfoMapper.deleteByPrimaryKey(id);

    }

    @Override
    public void update(Classinfo classinfo) {
        classinfoMapper.updateByPrimaryKey(classinfo);

    }

    @Override
    public Classinfo get(Long id) {
        Classinfo classinfo = classinfoMapper.selectByPrimaryKey(id);
        return classinfo;
    }

    @Override
    public List<Classinfo> listAll() {
        List<Classinfo> classinfos = classinfoMapper.selectAll();
        return classinfos;
    }

    public PageInfo query(QueryObject qo){
        //PageHelper分页插件
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Classinfo> classinfos = classinfoMapper.selectForList(qo);
        return new PageInfo(classinfos);
    }
}
