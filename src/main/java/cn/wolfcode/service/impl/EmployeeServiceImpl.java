package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Employee;
import cn.wolfcode.mapper.EmployeeMapper;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.qo.ResultPage;
import cn.wolfcode.service.IEmployeeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Service
public class EmployeeServiceImpl implements IEmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    //是用注解 set方法可以省略不写
    public void setEmployeeMapper(EmployeeMapper employeeMapper) {
        this.employeeMapper = employeeMapper;
    }

    @Override
    public void save(Employee employee, Long[] ids) {
        //保存员工信息之前要进行密码加密处理  (用户名作为盐)
        Md5Hash md5Hash = new Md5Hash(employee.getPassword(),employee.getName());
        employee.setPassword(md5Hash.toString());

        //保存员工信息
        employeeMapper.insert(employee);
        //保存员工角色中间表信息
        if (ids != null && ids.length > 0) {
            /*
            //循环遍历,多条sql
            for (Long rid : ids) {
                employeeMapper.insertRelation(employee.getId(),rid);
            }*/

            //使用批量增加的方式     练习一下    数据过多的话不推荐使用,会导致SQL语句过长
            employeeMapper.insertRelation2(employee.getId(), ids);
        }
    }

    @Override
    public void delete(Long id) {
        employeeMapper.deleteByPrimaryKey(id);
        //同时删除员工角色中间表中的无用信息
        employeeMapper.deleteRelation(id);
    }

    @Override
    public void update(Employee employee, Long[] ids) {
        //修改员工信息,然后拿到更新后的id值,去修改中间表数据
        employeeMapper.updateByPrimaryKey(employee);
        //需要先把员工角色关系删除掉
        employeeMapper.deleteRelation(employee.getId());
        if (ids != null && ids.length > 0) {
            //建立新的员工角色关系
            employeeMapper.insertRelation2(employee.getId(), ids);
        }
    }

    @Override
    public Employee get(Long id) {
        Employee employee = employeeMapper.selectByPrimaryKey(id);
        return employee;
    }

    @Override
    public List<Employee> listAll() {
        List<Employee> list = employeeMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo query(QueryObject qo) {
        //PageHelper分页插件
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<Employee> employees = employeeMapper.selectForList(qo);
        return new PageInfo(employees);
    }

    @Override
    public Employee login(String username, String password) {

        //根据username和password去数据库中查找,是否有相同的
        Employee employee = employeeMapper.selectByUsernameAndPassword(username, password);
        if (employee == null) {
            throw new RuntimeException("账号或密码错误!");
        }
        if (!employee.isStatus()) {
            throw new RuntimeException("账号被禁用,请联系管理员!");
        }
        return employee;
    }

    @Override
    public void updatePwd(Long id, String newPassword) {

        employeeMapper.updatePwdById(id, newPassword);
    }


    @Override
    public void batchDelete(Long[] ids) {
        employeeMapper.batchDelete(ids);
    }

    @Override
    public Employee selectByName(String name) {
        Employee employee =  employeeMapper.selectByName(name);
        return employee;
    }

    @Override
    public void updateStatus(Long id) {
        employeeMapper.updateStatus(id);
    }

    @Override
    public Workbook exportXls() {
        //查询所有的员工数据
        List<Employee> employees = employeeMapper.selectAll();
        //创建工作簿workbook (excel文件)
        Workbook workbook = new HSSFWorkbook();
        //创建一张表 (工作表)
        Sheet sheet = workbook.createSheet("员工通讯录");
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("姓名");
        row.createCell(1).setCellValue("邮箱");
        row.createCell(2).setCellValue("年龄");
        for(int i=0;i<employees.size();i++){
            //创建一行  形参是行的下标(从0开始)
            row = sheet.createRow(i+1);
            //创建单元格 形参是列的下标(从0开始)   并设置内容
            row.createCell(0).setCellValue(employees.get(i).getName());
            row.createCell(1).setCellValue(employees.get(i).getEmail());
            row.createCell(2).setCellValue(employees.get(i).getAge());
        }
        return workbook;
    }

    @Override
    public void importXls(MultipartFile file) throws IOException {
        //读Excel文件
        Workbook workbook = new HSSFWorkbook(file.getInputStream());
        //读一个表  不是read方法就是get方法
        Sheet sheet = workbook.getSheetAt(0);
        //获取表中最后一行数的索引
        int lastRowNum = sheet.getLastRowNum();
        for (int i=1;i<=lastRowNum;i++){
            Employee employee = new Employee();
            //读一行   标题行索引0不读
            Row row = sheet.getRow(i);
            String name = row.getCell(0).getStringCellValue();
            employee.setName(name);
            System.out.println(name);
            String email = row.getCell(1).getStringCellValue();
            employee.setEmail(email);
            if(row.getCell(2).getCellType().equals(CellType.STRING)){
                //文本格式的单元格格式
                String age = row.getCell(2).getStringCellValue();
                employee.setAge(Integer.getInteger(age));
            }else{
                //数字类型的单元格格式
                double age = row.getCell(2).getNumericCellValue();
                employee.setAge((int) age);
            }
            //设置默认密码
            employee.setPassword("1");
            //保存到数据库    不能用Mapper.insert  因为密码需要加密
            //employeeMapper.insert(employee);
            //调用当前类中的save方法,this可以不写,第二个参数是中间表的角色id数组,可以设置为null
            this.save(employee,null);
        }
    }

    @Override
    public List<Employee> selectByRoleSn(String... sns) {
        List<Employee> employees =  employeeMapper.selectByRoleSn(sns);
        return employees;
    }


    public List<Employee> selectByClassinfo() {
       List<Employee> employees =  employeeMapper.selectByClassinfo();
        return employees;
    }
}
