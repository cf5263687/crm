package cn.wolfcode.service.impl;

import cn.wolfcode.domain.SystemDictionaryItem;
import cn.wolfcode.mapper.SystemDictionaryItemMapper;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.service.ISystemDictionaryItemService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemDictionaryItemServiceImpl implements ISystemDictionaryItemService {

    @Autowired
    private SystemDictionaryItemMapper systemDictionaryItemMapper;
    //是用注解 set方法可以省略不写
    public void setSystemDictionaryItemMapper(SystemDictionaryItemMapper systemDictionaryItemMapper) {
        this.systemDictionaryItemMapper = systemDictionaryItemMapper;
    }

    @Override
    public void save(SystemDictionaryItem systemDictionaryItem) {
        systemDictionaryItemMapper.insert(systemDictionaryItem);
    }

    @Override
    public void delete(Long id) {
        systemDictionaryItemMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(SystemDictionaryItem systemDictionaryItem) {
        systemDictionaryItemMapper.updateByPrimaryKey(systemDictionaryItem);
    }

    @Override
    public SystemDictionaryItem get(Long id) {
        SystemDictionaryItem systemDictionaryItem = systemDictionaryItemMapper.selectByPrimaryKey(id);
        return systemDictionaryItem;
    }

    @Override
    public List<SystemDictionaryItem> listAll() {
        List<SystemDictionaryItem> list = systemDictionaryItemMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo query(QueryObject qo) {
        //使用分页插件,传入当前页,每页显示条数
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<SystemDictionaryItem> systemDictionaryItems = systemDictionaryItemMapper.selectForList(qo);
        return new PageInfo(systemDictionaryItems);
    }

    @Override
    public List<SystemDictionaryItem> selectBySn(String sn) {
        List<SystemDictionaryItem> systemDictionaryItems = systemDictionaryItemMapper.selectBySn(sn);
        return systemDictionaryItems;
    }

    @Override
    public int selectSequenceByParentId(Long parentId) {
       int sequence =  systemDictionaryItemMapper.selectSequenceByParentId(parentId);
        return sequence;
    }


}
