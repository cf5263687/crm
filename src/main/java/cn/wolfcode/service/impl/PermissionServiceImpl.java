package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Permission;
import cn.wolfcode.mapper.PermissionMapper;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.qo.ResultPage;
import cn.wolfcode.service.IPermissionService;
import cn.wolfcode.util.RequiredPermission;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class PermissionServiceImpl implements IPermissionService {

    @Autowired
    private PermissionMapper permissionMapper;
    //是用注解 set方法可以省略不写
    public void setPermissionMapper(PermissionMapper permissionMapper) {
        this.permissionMapper = permissionMapper;
    }

    @Override
    public void save(Permission dept) {
        permissionMapper.insert(dept);
    }

    @Override
    public void delete(Long id) {
        permissionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(Permission dept) {
        permissionMapper.updateByPrimaryKey(dept);
    }

    @Override
    public Permission get(Long id) {
        Permission dept = permissionMapper.selectByPrimaryKey(id);
        return dept;
    }

    @Override
    public List<Permission> listAll() {
        List<Permission> list = permissionMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Permission> permissions = permissionMapper.selectForList(qo);
        return new PageInfo(permissions);
    }

    @Autowired
    private ApplicationContext ctx; //spring上下文对象

    @Override
    public void reload() {
        //从数据库中获取所有的权限表达式    不是权限对象
        List<String> expressions = permissionMapper.selectAllExpression();
        //把所有的controller中贴了权限注解的方法,转换成权限对象,并保存到数据库中
        //利用spring上下文对象,根据controller注解从容器中获取多个bean对象      只要贴了controller注解的bean都可以拿到
        Map<String, Object> beans = ctx.getBeansWithAnnotation(Controller.class);
        //Map集合中所有的value,也就是bean对象
        Collection<Object> values = beans.values();
        //遍历 获取到每个controller对象
        for (Object controller : values) {
            //获取controller字节码对象
            //使用的是CGLIB动态代理     所以这里的应该是获取父类的controller字节码对象
            Class<?> controllerClass = controller.getClass().getSuperclass();
            //获取每个controller中的方法
            Method[] methods = controllerClass.getDeclaredMethods();
            //遍历每个方法
            for (Method method : methods) {
                //判断方法上是否贴有权限注解
                RequiresPermissions annotation = method.getAnnotation(RequiresPermissions.class);
                //判断注解是否为空
                if (annotation != null){
                    //获取注解中的权限表达式
                    String expression = annotation.value()[0];//value值是一个数组,取第一个元素
                    //判断数据库中是否已存在该权限对象,若不存在则保存到数据库中
                    if(!expressions.contains(expression)){
                        //获取注解中的权限名称
                        String name = annotation.value()[1];//value值是一个数组,取第二个元素
                        //封装成权限对象
                        Permission permission = new Permission();
                        permission.setName(name);
                        permission.setExpression(expression);
                        //把权限对象保存到数据库中
                        permissionMapper.insert(permission);
                    }
                }
                //如果为空,就不用处理了
            }
        }
    }

    @Override
    public List<String> selectExpressionsByEmpId(Long id) {
        List<String> expressions = permissionMapper.selectExpressionsByEmpId(id);
        return expressions;
    }
}
