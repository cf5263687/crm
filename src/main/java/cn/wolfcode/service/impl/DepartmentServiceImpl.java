package cn.wolfcode.service.impl;

import cn.wolfcode.domain.Department;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.qo.ResultPage;
import cn.wolfcode.mapper.DepartmentMapper;
import cn.wolfcode.service.IDepartmentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
@Service
public class DepartmentServiceImpl implements IDepartmentService {

    @Autowired
    private DepartmentMapper departmentMapper;
    //是用注解 set方法可以省略不写
    public void setDepartmentMapper(DepartmentMapper departmentMapper) {
        this.departmentMapper = departmentMapper;
    }

    @Override
    public void save(Department department) {
        departmentMapper.insert(department);
    }

    @Override
    public void delete(Long id) {
        departmentMapper.delete(id);
    }

    @Override
    public void update(Department department) {
        departmentMapper.update(department);
    }

    @Override
    public Department get(Long id) {
        Department department = departmentMapper.selectOne(id);
        return department;
    }

    @Override
    public List<Department> listAll() {
        List<Department> list = departmentMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo query(QueryObject qo) {
        //使用分页插件,传入当前页,每页显示条数
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Department> departments = departmentMapper.selectForList(qo);
        return new PageInfo(departments);
    }


}
