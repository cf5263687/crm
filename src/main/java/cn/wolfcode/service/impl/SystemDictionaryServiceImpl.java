package cn.wolfcode.service.impl;

import cn.wolfcode.domain.SystemDictionary;
import cn.wolfcode.mapper.SystemDictionaryMapper;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.service.ISystemDictionaryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemDictionaryServiceImpl implements ISystemDictionaryService {

    @Autowired
    private SystemDictionaryMapper systemDictionaryMapper;
    //是用注解 set方法可以省略不写
    public void setSystemDictionaryMapper(SystemDictionaryMapper systemDictionaryMapper) {
        this.systemDictionaryMapper = systemDictionaryMapper;
    }

    @Override
    public void save(SystemDictionary systemDictionary) {
        systemDictionaryMapper.insert(systemDictionary);
    }

    @Override
    public void delete(Long id) {
        systemDictionaryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(SystemDictionary systemDictionary) {
        systemDictionaryMapper.updateByPrimaryKey(systemDictionary);
    }

    @Override
    public SystemDictionary get(Long id) {
        SystemDictionary systemDictionary = systemDictionaryMapper.selectByPrimaryKey(id);
        return systemDictionary;
    }

    @Override
    public List<SystemDictionary> listAll() {
        List<SystemDictionary> list = systemDictionaryMapper.selectAll();
        return list;
    }

    @Override
    public PageInfo query(QueryObject qo) {
        //使用分页插件,传入当前页,每页显示条数
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<SystemDictionary> systemDictionarys = systemDictionaryMapper.selectForList(qo);
        return new PageInfo(systemDictionarys);
    }


}
