package cn.wolfcode.service;

import cn.wolfcode.domain.SystemDictionaryItem;
import cn.wolfcode.qo.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface ISystemDictionaryItemService {

    void save(SystemDictionaryItem systemDictionaryItem);
    void delete(Long id);
    void update(SystemDictionaryItem systemDictionaryItem);
    SystemDictionaryItem get(Long id);
    List<SystemDictionaryItem> listAll();

    PageInfo<SystemDictionaryItem> query(QueryObject qo);

    /**
     * 根据目录编码查询明细
     * @param sn
     * @return
     */
    List<SystemDictionaryItem> selectBySn(String sn);

    /**
     * 根据目录id查询明细的最大序号
     * @param parentId
     * @return
     */
    int selectSequenceByParentId(Long parentId);
}
