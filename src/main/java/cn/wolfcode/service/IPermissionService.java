package cn.wolfcode.service;

import cn.wolfcode.domain.Permission;
import cn.wolfcode.qo.QueryObject;
import cn.wolfcode.qo.ResultPage;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IPermissionService {

    void save(Permission dept);
    void delete(Long id);
    void update(Permission dept);
    Permission get(Long id);
    List<Permission> listAll();

    PageInfo<Permission> query(QueryObject qo);

    void reload();

    /**
     * 根据员工id查询员工拥有的权限
     * @param id    员工id
     * @return  员工拥有的权限列表
     */
    List<String> selectExpressionsByEmpId(Long id);
}
