package cn.wolfcode.service;

import cn.wolfcode.qo.CustomerReportQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

public interface ICustomerReportService {

    PageInfo query(CustomerReportQueryObject qo);

    List<Map> selectAll(CustomerReportQueryObject qo);
}
