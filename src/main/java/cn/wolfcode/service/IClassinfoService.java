package cn.wolfcode.service;

import cn.wolfcode.domain.Classinfo;

import java.util.List;

public interface IClassinfoService {
    void save(Classinfo classinfo);
    void delete(Long id);
    void update(Classinfo classinfo);
    Classinfo get(Long id);
    List<Classinfo> listAll();
}
