package cn.wolfcode.qo;

import cn.wolfcode.util.DateUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public class CustomerReportQueryObject extends QueryObject {
    private String groupType = "e.name";//默认使用员工分组  年 月 日分组从前端传值过来
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    public Date getEndDate(){
        //获取当天最后的时刻  23:59:59
        return DateUtil.getEndDate(endDate);
    }

    //需要动态获取到 中文 的分组类型
    public String getGroupTypeName(){
        switch(groupType){
            case "DATE_FORMAT(c.input_time, '%Y')":
                return "年份";
            case "DATE_FORMAT(c.input_time, '%Y-%m')":
                return "月份";
            case "DATE_FORMAT(c.input_time, '%Y-%m-%d')":
                return "日期";
            default:
                return "员工";
        }
    }

}
