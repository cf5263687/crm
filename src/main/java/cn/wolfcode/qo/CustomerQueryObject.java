package cn.wolfcode.qo;

import lombok.Data;

@Data
public class CustomerQueryObject extends QueryObject {
    private Integer status = -1;    //状态
    private Long sellerId = -1L;    //销售员
}
