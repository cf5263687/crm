package cn.wolfcode.qo;

import lombok.Data;

@Data
public class ClassinfoQueryObject extends QueryObject {
    private Long employeeId;
}
