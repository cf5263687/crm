package cn.wolfcode.qo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SystemDictionaryItemQueryObject extends QueryObject {
    private Long parentId = -1L;
}
