package cn.wolfcode.qo;

import lombok.Getter;

import java.util.List;
@Getter
public class ResultPage<T> {
    //客户传递过来的数据
    private Integer currentPage;    //当前页
    private Integer pageSize;       //页面显示的条数

    //查询出来的数据
    private Integer rows;       //总条数
    private List<?> result;     //结果集

    //计算出来的数据
    private Integer totalPage;
    private Integer prevPage;
    private Integer nextPage;


    public ResultPage() {
    }

    public ResultPage(Integer currentPage, Integer pageSize, Integer rows, List<T> result) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.rows = rows;
        this.result = result;

        this.totalPage = rows % pageSize == 0 ? rows / pageSize:rows / pageSize +1;    //总页数
        this.prevPage = currentPage > 1 ? currentPage-1 : currentPage;   //上一页
        this.nextPage = currentPage < totalPage ? currentPage + 1 : currentPage; //下一页
    }

}
