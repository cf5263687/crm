package cn.wolfcode.qo;

import lombok.Data;

@Data
public class CourseOrderQueryObject extends QueryObject {
    private Long courseId;  //课程
}
