package cn.wolfcode.qo;

import lombok.Data;

@Data
public class EmployeeQueryObject extends QueryObject {
    private Long deptId = -1L;
}
