import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ShiroTest {

    @Test
    public void testMD5(){
        Md5Hash md5Hash1 = new Md5Hash();//空参
        Md5Hash md5Hash2 = new Md5Hash("1");//一个object参数   给密码加密
        System.out.println(md5Hash2);   //c4ca4238a0b923820dcc509a6f75849b
        //两个object参数   给密码进行加盐加密   每个用户要用不同的盐  如:手机号,邮箱,不能有重复的用户名等
        Md5Hash md5Hash3 = new Md5Hash("1","admin");
        System.out.println(md5Hash3);   //e00cf25ad42683b3df678c61f42c6bda
        //两个object参数   给密码进行加盐加密   每个用户要用不同的盐  如:手机号,身份证号,不能有重复的用户名等
        Md5Hash md5Hash4 = new Md5Hash("1","15230073460");
        Md5Hash md5Hash5 = new Md5Hash("1","15230073460");
        System.out.println(md5Hash4);   //ad5d715e83d189e535dd60dfa5bf32e8
        System.out.println(md5Hash5);   //ad5d715e83d189e535dd60dfa5bf32e8
        //密码都是 1 , 不加盐和加盐的结果不一样,加的盐相同则结果也会相同  所以不同用户要加不同的盐
    }
    @Test
    public void testLogin() {
        //暂时不读数据库,提供假数据shiro.ini
        //构造shiro的环境（安全管理器）
        IniSecurityManagerFactory factory =
                new IniSecurityManagerFactory("classpath:shiro.ini");
        //创建shiro的安全管理器
        SecurityManager manager = factory.getInstance();
        //将创建的安全管理器添加到运行环境中
        SecurityUtils.setSecurityManager(manager);
        //获取当前的主体（用户）
        //无论是否登录，都可以拿到该对象
        Subject subject = SecurityUtils.getSubject();
        //根据isAuthenticated方法判断认证的状态
        System.out.println("认证状态：" + subject.isAuthenticated());
        //创建登录的令牌Token
        UsernamePasswordToken token = new UsernamePasswordToken("lisi", "666");
        //执行登录
        subject.login(token);
        System.out.println("认证状态：" + subject.isAuthenticated());
        //判断用户是否拥有role1的角色
        System.out.println("判断用户是否拥有role1的角色" + subject.hasRole("role1"));
        //判断用户是否拥有role2的角色
        System.out.println("判断用户是否拥有role2的角色" + subject.hasRole("role2"));
        //判断用户是否同时拥有role1和role2的角色
        List<String> strings = Arrays.asList("role1", "role2");
        System.out.println("判断用户是否同时拥有role1和role2的角色" + subject.hasAllRoles(strings));
        //checkRole()   checkPremission() check方法进行判断是没有返回值的,如果没有拥有角色/权限就抛异常
        //subject.checkRole("role1");//没有返回值,如果没有这个角色会直接抛异常
        //subject.checkRole("role2");
        //判断用户是否拥有user:update的权限
        System.out.println("判断用户是否拥有user:update的权限"+subject.isPermitted("user:update"));
        //判断用户是否拥有user:delete的权限
        System.out.println("判断用户是否拥有user:delete的权限"+subject.isPermitted("user:delete"));
        //执行注销
        subject.logout();
        System.out.println("认证状态：" + subject.isAuthenticated());
    }
}
